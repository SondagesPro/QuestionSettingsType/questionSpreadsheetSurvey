<?php
/** @version 3.0.0 **/
/** @depreacted replace by twig **/
?>
<?php
if(!empty($warnings)) {
    echo "<div class='alert alert-warning'>";
    echo "<strong>questionSpreadsheetSurvey errors</strong>";
    echo "<ul class=''>";
    foreach($warnings as $warning) {
        echo "<li>$warning</li>";
    }
    echo "</ul></div>";
}
?>
<?php 
echo \CHtml::tag("div",
    array(
        'id' => "jexcel".$inputName,
        'class' => 'speadsheet-element',
        'data-relatedSurvey' => $relatedSurvey,
        'data-inputid' => 'answer'.$inputName,
        'data-token' => $token,
        'data-columns' => json_encode($spreadsheetOptions['jexcel']['columns']),
        'data-updateurl' => $spreadsheetOptions['updateUrl'],
        'data-deleteurl' => $spreadsheetOptions['deleteUrl'],
        'data-resulturl' => $spreadsheetOptions['resultUrl'],
    ),
    ""
);
?>
<?php
    echo \CHtml::tag(
        "div",
        array(
            'class' => 'answer-item text-item hidden',
            'aria-hidden' => 'true',
            'title' => '',
        ),
        \CHtml::textField($inputName,$value,array(
            'class'=>'form-control',
            'id' => 'answer'.$inputName,
        ))
    );
?>
<script>
    $("#answer<?php echo $inputName?>").trigger("keyup");
    questionSpreadsheetSurvey.GlobalOptions = <?= json_encode($spreadsheetGlobalOptions) ?>;
    <?php
        $jsonSpreadSheetOptions = json_encode($spreadsheetOptions,JSON_PRETTY_PRINT);
        /* Replace js function call */
        /* Not needed , leave it if needed in future */
        $jsonSpreadSheetOptions = str_replace(
            array(
                '"questionSpreadsheetSurvey.functionname"',
            ),
            array(
                'questionSpreadsheetSurvey.functionname',
            ),
            $jsonSpreadSheetOptions);
    ?>
    questionSpreadsheetSurvey.setSpreadsheet("jexcel<?= $inputName ?>",<?= $jsonSpreadSheetOptions ?>);
</script>

