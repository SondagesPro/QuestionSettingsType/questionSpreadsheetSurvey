# questionSpreadsheetSurvey

Use a question to add survey inside survey visually like a spreadsheet

## Installation

This plugin is tested with LimeSurvey 3.22 and 3.25

### Via GIT
- Go to your LimeSurvey Directory
- Clone in plugins/questionExtraSurvey directory `git clone https://gitlab.com/SondagesPro/QuestionSettingsType/questionSpreadsheetSurvey.git questionSpreadsheetSurvey`

### Via ZIP dowload
- Download <https://dl.sondages.pro/questionSpreadsheetSurvey.zip>
- Extract : `unzip questionSpreadsheetSurvey.zip`
- Move the directory to  plugins/ directory inside LimeSUrvey

## Usage

### Example and demonstration

- [Demo : Anonym survey with SpreadSheetSurvey](https://demo.sondages.pro/896317)

### Basic usage with token enables survey's

The plugin is tested for survey with token, not anonymous, token-based response persistence and allow edit response activated.

The plugin use 2 surveys , 2nd one are included in the 2st one

The settings can be set on text display question type or long text question type. With long text question type you can save the number of valid lines and invalid lines.

#### Question settings
- `spreadsheetSurvey` _Survey to use_ : the survey id of the second survey 
- `spreadsheetSurveyFillAnswer` _Way for filling this question_ : default one allow you to make clean control on data set. You can choose to have lits of related id.
- `spreadsheetSurveyQuestionLink` _Optional question code to get the reponse id_ : the question title for the current response id. This option are not necessary for functionnal system. 
- `spreadsheetSurveyOtherField` _Other question fields for relation._ : You can use a [generateUniqId](https://gitlab.com/SondagesPro/QuestionSettingsType/generateUniqId) question for example
- `spreadsheetSurveyTokenUsage` _Usage of token_ : Use token in relation for not anonymous survey.
- `spreadsheetSurveyOrderBy` _The default order_ : youn can use supported question title only. Remind user can set is own order after loading.
- `spreadsheetSurveyForcedFillQuestion` _Question title for prefilling_ : You can prefill values of one question in related survey.
- `spreadsheetSurveyForcedFillValues` _The values to be prefilled_ : can use Expression manager. If set this disable adding new lines. When created the reponse have a datestamp of 1980-01-01.
- `spreadsheetSurveyForcedSubmitted` _Prefilled values set as submitted_ : set submitted value when prefilll, control of data are not done.
- `spreadsheetSurveyDeleteNotForcedValues` _Remove extra values_  only in case of prefilling, unsure to have only prefilled values.
- `spreadsheetSurveyFixedLines` _disable adding or remove response in related survey_, you must prefill using another solution (for example VV import)
- `spreadsheetSurveyHiddenQuestions` extra questions to be hidden  To hide question, you can use hidden attribute, but if needed you can hide it here too. Use , as separator.

#### related survey available question and restrictions

- Text question
    - Short text question are single line
    - Long anb ghuge question can use newline, to entre a new line: participant must use `[ALT]+[ENTER]`
- Single choice question
    - Show as dropdown
- Yes no question
    - Show as checkbox
- Numeric question
    - integer attribute allow dot or not
- Date question
    - No specific date format allowed, only `Y-m-d` supported
- Equation
    - Shown as readonly

You can use hidden attribute or readonly (via [answersAsReadonly](https://gitlab.com/SondagesPro/QuestionSettingsType/answersAsReadonly)) attribute .

All logic of all question are checked like is done for live survey, if survey can be submitted : it was submitted. If survey logic have error : line are shown with a warning color.

### Improve relations between related survey using any questions field for relation

When export, import first survey, or if you need to deactivate the first survey. When you reload previous response table : the link between reponse are totally lost.

You can use a [generateUniqId](https://gitlab.com/SondagesPro/QuestionSettingsType/generateUniqId) question for the link between surveys.

1. Create you uniqId question in the first survey (title : uniqId here)
2. Set _Other question fields for relation_ to surveyLinkSrid:{uniqId.NAOK}

You can use any question, for example : you can use TOKEN.

### More solution for javascript and HTML

- Question text is not filtered when set to head cell, then you can use any html element
- By default question text are filtered for title of the head cell
- If question have help : it's set as title of the header cell
- When jexcel data are loaded : jquery event `jexcelloaded`, then you can use `$(".speadsheet-element").on('jexcelloaded',function() { });` to do your own work on data.
- There are a workaround to allow simple click on dropdow, to disable this workaround : see `.jexcel > tbody > tr > td.dropdown` class in css and add `.jexcel > tbody > tr > td.dropdown .dropdown-open {display:none}`
- You can use you own twig file for answer part using `/survey/questions/answer/spreadsheetsurvey/answer.twig`

## Home page & Copyright
- HomePage <https://extensions.sondages.pro/>
- Code repository <https://gitlab.com/SondagesPro/QuestionSettingsType/questionSpreadsheetSurvey>
- Copyright © 2020-2021 Denis Chenu <www.sondages.pro>
- Copyright © 2020-2021 OECD (Organisation for Economic Co-operation and Development ) <www.oecd.org>
- [jexcel CE](https://bossanova.uk/jexcel/v4/) and [jsuites](https://bossanova.uk/jsuites/) are Copyright © Paul Hodel and distributed ender MIT licence
- [![Donate](https://liberapay.com/assets/widgets/donate.svg)](https://liberapay.com/SondagesPro/) : [Donate on Liberapay](https://liberapay.com/SondagesPro/)

Distributed under [GNU GENERAL PUBLIC LICENSE Version 3](https://gnu.org/licenses/gpl-3.0.txt) licence
