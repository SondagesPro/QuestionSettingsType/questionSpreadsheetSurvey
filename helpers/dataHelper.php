<?php

/**
 * Description
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2020-2024 Denis Chenu <http://www.sondages.pro>
 * @license AGPL v3
 * @version 2.2.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

namespace questionSpreadsheetSurvey\helpers;

use Yii;
use CHttpException;
use CHtml;
//~ use PDO;
use Survey;
use CDbCriteria;
/* Question information */
use Question;
use QuestionL10n;
use QuestionAttribute;
use Answer;
/* Get tha data */
use Response;
use SurveyDynamic;
/* Expression */
use LimeExpressionManager;

class dataHelper
{
    /** @var boolean : debug (show some columns etc ..) **/
    public $debug = false;

    /** @var null|integer **/
    public $surveyId = null;
    /** @var null|string **/
    public $language = null;

    /** @var array */
    public $restrictedTo = [];

    /** @var string|null */
    public $token = null;

    /** @var string[] */
    public $tokens = [];

    /** @var null|string **/
    public $orderBy = null;

    /** @var string[] Columns forced as readonly**/
    private $readonlyColumns = [];

    /** @var array sgqa=>['op'=>op,'value'=>value] for conditions read only line**/
    private $readonlyConditions = [];
    /** @var array sgqa=>['op'=>op,'value'=>value] for conditions read only line**/
    private $readonlySubmittedConditions = [];

    /** @var string[] Columns forced as hidden **/
    public $hiddenColumns = [];

    /** @var string[] Specific columns to update with autocomplete **/
    public $autocompleteUpdates = [];

    /** @var string[] QTYPE Supported question type **/
    const QTYPE = [
        "S",
        "T",
        "U", // Text
        "Y", // Yes no : checkox
        "L",
        "!", // Single choice as dropdown
        "N", // Number
        "*", // Equation (show as readonly)
        "D", // Date
    ];

    /** @var boolean Add an empty line **/
    public $addEmptyLine = true;

    /**
     * array : columns information called for each line, use static to avoid SQL request
     */
    private $aColumns = null;

    /**
     * Constructor
     * @param integer $surveyId
     * @param string $language
     * @return void
     */
    public function __construct($surveyId, $language = null)
    {
        $this->surveyId = $surveyId;
        if (empty($language)) {
            $language = App()->getLanguage();
        }
        $this->language = $language;
        if (
            !in_array(
                $language,
                Survey::model()
                    ->findByPk($this->surveyId)
                    ->getAllLanguages()
            )
        ) {
            $this->language = Survey::model()->findByPk(
                $this->surveyId
            )->language;
        }
    }

    /**
     * Set the token and add it to restriction
     * @param $string
     * @return void
     */
    public function setToken($token)
    {
        $this->token = $token;
        $this->tokens[] = $token;
    }

    /**
     * Set restriction
     * @params string[] key as question code , value as restriction
     * @return integer : number of restriction added
     */
    public function setRestrictedTo($aRestricted)
    {
        if (empty($aRestricted)) {
            return 0;
        }
        $count = 0;
        $availableColumns = SurveyDynamic::model(
            $this->surveyId
        )->getAttributes();
        foreach ($aRestricted as $title => $value) {
            $sgq = $this->getColumnSgq($title);
            if ($sgq) {
                $this->restrictedTo[$sgq] = $value;
                $count++;
            } else {
                \Yii::log(
                    "Unable to find question $title in survey {$this->surveyId}",
                    \CLogger::LEVEL_WARNING,
                    "plugin.questionSpreadsheetSurvey.helpers.dataHelper.setRestrictedTo"
                );
            }
        }
        return $count;
    }

    /**
     * Set restriction
     * @params string[] key as question code , value as restriction
     * @params boolean on submitted
     * @return integer : number of condition added
     */
    public function setReadOnlyCondition($aConditions, $submitted = false)
    {
        if (empty($aConditions)) {
            return 0;
        }
        $count = 0;
        $availableColumns = SurveyDynamic::model(
            $this->surveyId
        )->getAttributes();
        foreach ($aConditions as $title => $value) {
            $sgq = $this->getColumnSgq($title);
            if ($sgq) {
                $op = "";
                if (preg_match('/^(?:\s*(<>|<=|>=|<|>|=))?(.*)$/', $value, $matches)) {
                    $value = trim($matches[2]);
                    $op = $matches[1];
                }
                if (!empty($value)) {
                    if ($submitted) {
                        $this->readonlySubmittedConditionsConditions[$sgq] = array(
                            'op' => $op,
                            'value' => $value
                        );
                    } else {
                        $this->readonlyConditions[$sgq] = array(
                            'op' => $op,
                            'value' => $value
                        );
                    }
                    $count++;
                }
            } else {
                \Yii::log(
                    "Unable to find question $title in survey {$this->surveyId}",
                    \CLogger::LEVEL_WARNING,
                    "plugin.questionSpreadsheetSurvey.helpers.dataHelper.setReadOnlyCondition"
                );
            }
        }
        return $count;
    }

    /**
     * Set the forced data
     * @params string $questionCode question code
     * @params string[] The values to set
     * @params boolean delete the extra response
     * @params boolean Create as submitted
     * @return void
     */
    public function setForcedDatas(
        $questionCode,
        $aValuesToSet,
        $bDeleteOtherValues = false,
        $bSubmitted = false
    ) {
        $criteria = new CDbCriteria();
        $criteria->compare("sid", $this->surveyId);
        $criteria->compare("parent_qid", 0);
        $criteria->compare("title", $questionCode);
        $criteria->addInCondition("type", self::QTYPE);
        $oForcedQuestion = \Question::model()->find($criteria);
        if (empty($oForcedQuestion)) {
            \Yii::log(
                "Unable to find question $questionCode in survey {$this->surveyId}",
                \CLogger::LEVEL_WARNING,
                "plugin.questionSpreadsheetSurvey.helpers.dataHelper.setForcedDatas"
            );
            return;
        }
        $questionSgq =
            $oForcedQuestion->sid .
            "X" .
            $oForcedQuestion->gid .
            "X" .
            $oForcedQuestion->qid;
        $aReponseIds = [];
        foreach ($aValuesToSet as $valueToSet) {
            /* To do : check validity of values */
            $oCriteria = new CDbCriteria();
            $oCriteria->select = [
                "id",
                App()->db->quoteColumnName($questionSgq),
            ];
            if (!empty($this->tokens)) {
                $oCriteria->addInCondition("token", $this->tokens);
            }
            foreach ($this->restrictedTo as $column => $value) {
                $oCriteria->compare(
                    App()->db->quoteColumnName($column),
                    $value
                );
            }
            $oCriteria->compare(
                App()->db->quoteColumnName($questionSgq),
                $valueToSet
            );
            $oResponse = Response::model($this->surveyId)->find($oCriteria);
            $oSurvey = Survey::model()->findByPk($this->surveyId);
            $currentDate = dateShift(
                (string) date("Y-m-d H:i:s"),
                "Y-m-d H:i:s",
                Yii::app()->getConfig("timeadjust")
            );
            if (empty($oResponse)) {
                $oResponse = Response::create($this->surveyId);
                // startlanguage
                $oResponse->startlanguage = $this->language;
                // datestamped ?
                foreach ($this->restrictedTo as $column => $value) {
                    $oResponse->setAttribute($column, $value);
                }
                if (!is_null($this->token)) {
                    $oResponse->setAttribute("token", $this->token);
                }
                $oResponse->setAttribute($questionSgq, $valueToSet);
                if ($oSurvey->getIsDateStamp()) {
                    $oResponse->setAttribute("startdate", $currentDate);
                    $oResponse->setAttribute(
                        "datestamp",
                        date("Y-m-d H:i:s", mktime(0, 0, 0, 1, 1, 1980))
                    ); // Auto created survey have a differeent datestamp
                }
                if ($bSubmitted) {
                    $submitdate = $oSurvey->getIsDateStamp()
                        ? $currentDate
                        : date("Y-m-d H:i:s", mktime(0, 0, 0, 1, 1, 1980));
                    $oResponse->setAttribute("submitdate", $submitdate);
                }
                if (!$oResponse->save()) {
                    throw new CHttpException(
                        500,
                        CHtml::errorSummary($oResponse)
                    );
                }
            }
            $aReponseIds[] = $oResponse->id;
        }
        /* And delete all other one */
        if ($bDeleteOtherValues && !empty($aReponseIds)) {
            $oDeleteCriteria = new CDbCriteria();
            if (!empty($this->tokens)) {
                $oDeleteCriteria->addInCondition("token", $this->tokens);
            }
            foreach ($this->restrictedTo as $column => $value) {
                $oDeleteCriteria->compare(
                    Yii::app()->db->quoteColumnName($column),
                    $value
                );
            }
            $oDeleteCriteria->addNotInCondition("id", $aReponseIds);
            $oResponse = Response::model($this->surveyId)->deleteAll(
                $oDeleteCriteria
            );
        }
    }

    /**
     * Set a column as readonly
     * @param string $title column code (question or specific)
     * @return booelan
     */
    public function setColumnsAsReadonly($title)
    {
        $sgq = $this->getColumnSgq($title);
        if (empty($sgq)) {
            return false;
        }
        $this->readonlyColumns[] = $sgq;
        return true;
    }

    /**
     * Set a column as hidden
     * @param string $title column code (question or specific)
     * @return booelan
     */
    public function setColumnsAsHidden($title)
    {
        $sgq = $this->getColumnSgq($title);
        if (empty($sgq)) {
            return false;
        }
        $this->hiddenColumns[] = $sgq;
        return true;
    }

    /**
     * Get the columns data
     * @param boolen $idAsKey
     * @return array[string[]]
     */
    public function getColumns($idAsKey = false)
    {
        if (!$idAsKey && $this->aColumns) {
            return $this->aColumns;
        }
        $criteria = new CDbCriteria();
        $criteria->with = ["group"];
        $criteria->order =
            App()->db->quoteColumnName("group.group_order") .
            " asc, " .
            App()->db->quoteColumnName("t.question_order") .
            " asc";
        $criteria->compare("t.sid", $this->surveyId);
        $criteria->compare("t.parent_qid", 0);
        $criteria->addInCondition("type", self::QTYPE);
        $oValidQuestions = \Question::model()->findAll($criteria);
        $aSurveyInfo = getSurveyInfo(
            $this->surveyId,
            $this->language
        ); /* A lot of things here ? */
        $aColumns = [];
        if ($idAsKey) {
            $aColumns["id"] = [
                "id" => "id",
                "type" => $this->debug ? "text" : "hidden",
                "title" => "srid",
                "readOnly" => true,
            ];
            $aColumns["state"] = [
                "id" => "state",
                "type" => $this->debug ? "text" : "hidden",
                "title" => "state",
                "readOnly" => true,
            ];
        } else {
            $aColumns[] = [
                "id" => "id",
                "type" => $this->debug ? "text" : "hidden",
                "title" => "srid",
                "readOnly" => true,
            ];
            $aColumns[] = [
                "id" => "state",
                "type" => $this->debug ? "text" : "hidden",
                "title" => "state",
                "readOnly" => true,
            ];
        }
        if ($idAsKey) {
            $aColumns["submitdate"] = [
                "id" => "submitdate",
                "type" => $this->debug ? "text" : "hidden",
                "title" => "completed",
                "readOnly" => true,
            ];
        } else {
            $aColumns[] = [
                "id" => "submitdate",
                "type" => $this->debug ? "text" : "hidden",
                "title" => "completed",
                "readOnly" => true,
            ];
        }
        foreach ($oValidQuestions as $oValidQuestion) {
            $aValidQuestionAttributes = $oValidQuestion->getAttributes();
            /* Language attributes */
            $aValidQuestionAttributes["question"] = "";
            $aValidQuestionAttributes["help"] = "";
            $oQuestionL10N = QuestionL10n::model()->find(
                "qid = :qid and language = :language",
                [":qid" => $oValidQuestion->qid, ":language" => $this->language]
            );
            if ($oQuestionL10N) {
                $aValidQuestionAttributes["question"] =
                    $oQuestionL10N->question;
                $aValidQuestionAttributes["help"] = $oQuestionL10N->help;
            }
            $renderData = [
                "aSurveyInfo" => $aSurveyInfo /* A lot of things here ? */,
                "aQuestion" => $aValidQuestionAttributes,
                "qid" => $oValidQuestion->qid,
                "sgq" =>
                    $oValidQuestion->sid .
                    "X" .
                    $oValidQuestion->gid .
                    "X" .
                    $oValidQuestion->qid,
            ];
            $aColumnInfo = [
                "id" =>
                    $oValidQuestion->sid .
                    "X" .
                    $oValidQuestion->gid .
                    "X" .
                    $oValidQuestion->qid,
                "qid" => $oValidQuestion->qid,
                "title" => App()->twigRenderer->renderPartial(
                    "/survey/questions/answer/spreadsheetsurvey/column_title.twig",
                    $renderData
                ),
                "titleattr" => $oValidQuestion->title,
                "allowEmpty" => true,
            ];
            /* @todo : check if need, maybe can disable send it to html */
            $hiddenAttribute = QuestionAttribute::model()->find(
                "qid = :qid and attribute = :attribute",
                [":qid" => $oValidQuestion->qid, ":attribute" => "hidden"]
            );
            if (!empty($hiddenAttribute) && $hiddenAttribute->value) {
                continue;
            }
            /* Use answersAsReadonly plugin */
            $readonly = false;
            $readonlyAttribute = QuestionAttribute::model()->find(
                "qid = :qid and attribute = :attribute",
                [":qid" => $oValidQuestion->qid, ":attribute" => "readonly"]
            );
            if (!empty($readonlyAttribute) && $readonlyAttribute->value) {
                $readonly = true;
            }
            if (in_array($aColumnInfo["id"], $this->readonlyColumns)) {
                $readonly = true;
            }

            switch ($oValidQuestion->type) {
                case "N":
                    $aColumnInfo["type"] = "numeric";
                    $aAttributeIntOnly = QuestionAttribute::model()->getQuestionAttributes(
                        $oValidQuestion->qid
                    );
                    $integeronlyAttribute = QuestionAttribute::model()->find(
                        "qid = :qid and attribute = :attribute",
                        [
                            ":qid" => $oValidQuestion->qid,
                            ":attribute" => "num_value_int_only",
                        ]
                    );
                    //$aColumnInfo['editor'] = 'questionSpreadsheetSurvey.numberCell';
                    //$aColumnInfo['editor'] = 'questionSpreadsheetSurvey.numeric';
                    $aColumnInfo["mask"] = "";
                    if (
                        !empty($integeronlyAttribute) &&
                        $integeronlyAttribute->value
                    ) {
                        $aColumnInfo["mask"] = "#";
                    }
                    $aColumnInfo["decimal"] = ".";
                    $aColumnInfo["readOnly"] = $readonly;
                    break;
                case "L":
                case "!":
                    $aColumnInfo["type"] = "dropdown";
                    $oAnswers = \Answer::model()
                        ->with("answerl10ns")
                        ->findAll([
                            "condition" => "qid = :qid",
                            "params" => [":qid" => $oValidQuestion->qid],
                        ]);
                    $options = [];
                    foreach ($oAnswers as $oAnswer) {
                        $name = "";
                        if (!empty($oAnswer->answerl10ns[$this->language])) {
                            $name = $oAnswer->answerl10ns[$this->language]->answer;
                        }
                        $options[] = [
                            "id" => $oAnswer->code,
                            "name" => $name,
                        ];
                    }
                    $aColumnInfo["source"] = $options;
                    $aColumnInfo["readOnly"] = $readonly;
                    break;
                case "*":
                    $aColumnInfo["type"] = "text";
                    $aColumnInfo["readOnly"] = true;
                    break;
                case "S":
                    $relatedSpreadsheetResponseCompleteAttribute = QuestionAttribute::model()->find(
                        "qid = :qid AND attribute = :attribute",
                        [":qid" => $oValidQuestion->qid, ":attribute" => 'relatedSpreadsheetResponseComplete']
                    );
                    if (!empty($relatedSpreadsheetResponseCompleteAttribute) && trim($relatedSpreadsheetResponseCompleteAttribute->value)) {
                        $aColumnInfo["type"] = "dropdown";
                        $aColumnInfo["url"] = App()->urlManager->createUrl(
                            "plugins/direct",
                            [
                                "plugin" => 'questionSpreadsheetSurvey',
                                'function' => 'autocomplete',
                                'qid' => $oValidQuestion->qid,
                                'token' => $this->token,
                                'lang' => $this->language
                            ]
                        );
                        $aColumnInfo["autocomplete"] = true;
                        $aColumnInfo["multiple"] = false;
                        /* @Todo use lazyLoading and remoteSearch to minimize size of data, but find the way to send current data at 1st load */
                        //~ $aColumnInfo["lazyLoading"] = true;
                        //~ $aColumnInfo["remoteSearch"] = true;
                    } else {
                        $aColumnInfo["type"] = "text";
                        $aColumnInfo["wordWrap"] = false;
                    }
                    $aColumnInfo["readOnly"] = $readonly;
                    break;
                case "T":
                case "H":
                    $aColumnInfo["type"] = "text";
                    $aColumnInfo["wordWrap"] = true;
                    $aColumnInfo["readOnly"] = $readonly;
                    break;
                case "Y":
                    $aColumnInfo["type"] = "checkbox";
                    $aColumnInfo["readOnly"] = $readonly;
                    break;
                case "D":
                    $aColumnInfo["type"] = "calendar";
                    $aColumnInfo["readOnly"] = $readonly;
                    break;
                default:
                    $aColumnInfo["type"] = "text";
                    $aColumnInfo["readOnly"] = $readonly;
            }
            /* Force hidden attribute */
            $hiddenAttribute = QuestionAttribute::model()->find(
                "qid = :qid and attribute = :attribute",
                [":qid" => $oValidQuestion->qid, ":attribute" => "hidden"]
            );
            if (!empty($hiddenAttribute) && $hiddenAttribute->value) {
                $aColumnInfo["type"] = "hidden";
            }
            if (in_array($aColumnInfo["id"], $this->hiddenColumns)) {
                $aColumnInfo["type"] = "hidden";
            }
            if ($idAsKey) {
                $aColumns[$aColumnInfo["id"]] = $aColumnInfo;
            } else {
                $aColumns[] = $aColumnInfo;
            }
        }
        if (!$idAsKey) {
            $this->aColumns = $aColumns;
        }
        return $aColumns;
    }

    /**
     * get data
     * @return array[array]
     */
    public function getData()
    {
        $oCriteria = new CDbCriteria();
        if (version_compare(App()->getConfig('RelatedSurveyManagementApiVersion', "0.0"), "0.9", ">=")) {
            $RelatedSurveysHelper = \RelatedSurveyManagement\RelatedSurveysHelper::getInstance($this->surveyId);
            $oCriteria = $RelatedSurveysHelper->addFilterDeletedCriteria($oCriteria);
        }
        $aSelect = [];
        $aSelected = [];
        foreach ($this->getColumns() as $column) {
            if ($column["id"] != 'state') {
                $aSelect[$column["id"]] = App()->db->quoteColumnName(
                    $column["id"]
                );
            }
            $aSelected[] = $column["id"];
        }
        $oCriteria->select = $aSelect;
        if (!empty($this->tokens)) {
            $oCriteria->addInCondition("token", $this->tokens);
        }
        foreach ($this->restrictedTo as $column => $value) {
            $oCriteria->compare(
                App()->db->quoteColumnName($column),
                $value
            );
        }
        $oCriteria->order = $this->getOrder();
        $oResponses = Response::model($this->surveyId)->findAll($oCriteria);
        $aResponses = [];
        if ($oResponses) {
            $aResponse = [];
            foreach ($oResponses as $oResponse) {
                $aResponses[] = $this->getResponseData($oResponse);
            }
        }
        if ($this->addEmptyLine) {
            /* Empty lines */
            $aNewResponse = [];
            foreach ($aSelected as $column) {
                $aNewReponse[$column] = "";
            }
            $aResponses[] = $aNewReponse;
        }
        return $aResponses;
    }

    /**
     * Delete a response if allowed
     * @param integer[] $srid
     * @return integer : number of row deleted (depend on DB system)
     */
    public function deleteData($srid)
    {
        $oCriteria = new CDbCriteria();
        if (!empty($this->tokens)) {
            $oCriteria->addInCondition("token", $this->tokens);
        }
        foreach ($this->restrictedTo as $column => $value) {
            $oCriteria->compare(
                Yii::app()->db->quoteColumnName($column),
                $value
            );
        }
        $oCriteria->addInCondition(
            Yii::app()->db->quoteColumnName("id"),
            $srid
        );
        if (version_compare(App()->getConfig('RelatedSurveyManagementApiVersion', "0.0"), "0.9", ">=")) {
            $RelatedSurveysHelper = \RelatedSurveyManagement\RelatedSurveysHelper::getInstance($this->surveyId);
            return $RelatedSurveysHelper->deleteResponse($oCriteria);
        }
        return Response::model($this->surveyId)->deleteAll($oCriteria);
    }

    /**
     * Get the result after updating data to fill answer part
     * @var string $type of result : ['number','listall','listsubmitted'], default 'number'
     * @return string
     */
    public function getResult($type = "number")
    {
        $oCriteria = new CDbCriteria();
        if (version_compare(App()->getConfig('RelatedSurveyManagementApiVersion', "0.0"), "0.9", ">=")) {
            $RelatedSurveysHelper = \RelatedSurveyManagement\RelatedSurveysHelper::getInstance($this->surveyId);
            $oCriteria = $RelatedSurveysHelper->addFilterDeletedCriteria($oCriteria);
        }
        $oCriteria->select = ["id"];
        if (!empty($this->tokens)) {
            $oCriteria->addInCondition("token", $this->tokens);
        }
        foreach ($this->restrictedTo as $column => $value) {
            $oCriteria->compare(
                Yii::app()->db->quoteColumnName($column),
                $value
            );
        }
        switch ($type) {
            default:
                $all = Response::model($this->surveyId)->count($oCriteria);
                $oCriteria->addCondition(
                    "submitdate <> '' AND submitdate IS NOT NULL"
                );
                $completed = Response::model($this->surveyId)->count(
                    $oCriteria
                );
                if ($completed == $all) {
                    return $completed;
                }
                $notcompleted = $all - $completed;
                return $completed . "." . $notcompleted;
            case "listall":
                $oResponses = Response::model($this->surveyId)->findAll(
                    $oCriteria
                );
                $aresult = CHtml::listData($oResponses, "id", "id");
                return implode(",", $aresult);
            case "listsubmitted":
                $oCriteria->addCondition(
                    "submitdate <> '' AND submitdate IS NOT NULL"
                );
                $oResponses = Response::model($this->surveyId)->findAll(
                    $oCriteria
                );
                $aresult = CHtml::listData($oResponses, "id", "id");
                return implode(",", $aresult);
        }
    }

    /**
     * Set the order by specific instruction
     * @param string
     * @return boolean success
     */
    public function setOrderBy($orderBy)
    {
        $success = true;
        if (empty($orderBy)) {
            return $success;
        }
        $aOrdersBy = explode(",", $orderBy);
        $aOrderByFinal = [];
        foreach ($aOrdersBy as $sOrderBy) {
            $aOrderBy = explode(" ", trim($sOrderBy));
            $arrangement = "ASC";
            if (!empty($aOrderBy[1]) and strtoupper($aOrderBy[1]) == "DESC") {
                $arrangement = "DESC";
            }
            if (!empty($aOrderBy[0])) {
                $orderColumn = null;
                $orderColumn = $this->getColumnSgq($aOrderBy[0]);
                if (empty($orderColumn)) {
                    $success = false;
                    continue;
                }
                $aOrderByFinal[] =
                    Yii::app()->db->quoteColumnName($orderColumn) .
                    " " .
                    $arrangement;
            }
        }
        $this->orderBy = implode(",", $aOrderByFinal);

        return $success;
    }

    /**
     * Get the order by instruction
     * @return string
     */
    private function getOrder()
    {
        $sFinalOrderBy = $this->orderBy;
        if (empty($sFinalOrderBy)) {
            $sFinalOrderBy = Yii::app()->db->quoteColumnName("id") . " DESC";
            if (Survey::model()->findByPk($this->surveyId)->datestamp == "Y") {
                $sFinalOrderBy =
                    Yii::app()->db->quoteColumnName("datestamp") . " ASC";
            }
        }
        return $sFinalOrderBy;
    }

    /**
     * Set data
     * @param int $srid, if null : create an new response
     * @param string[] $answers
     * @param boolena $submit
     * @return array : response=>array|null, submitted = date|true|null, status=> array , errors = // TODO
     */
    public function setData($srid = null, $answers = [], $submit = true)
    {
        $aReturn = [
            "response" => null,
            "submitted" => null,
            "status" => null,
            "errors" => null,
        ];
        $responseState = null;
        if (!empty($srid)) {
            $oCriteria = new CDbCriteria();
            if (!empty($this->tokens)) {
                $oCriteria->addInCondition("token", $this->tokens);
            }
            foreach ($this->restrictedTo as $column => $value) {
                $oCriteria->compare(
                    App()->db->quoteColumnName($column),
                    $value
                );
            }
            $oCriteria->compare(Yii::app()->db->quoteColumnName("id"), $srid);
            $oResponse = Response::model($this->surveyId)->find($oCriteria);
            if (empty($oResponse)) {
                /* @todo : log it as error (and understand how it happen) */
                //throw new \CHttpException(400,sprintf(gT("Invalid response id %s."), $srid));
                $srid = null;
                $newSrid = $srid;
            } else {
                $responseState = $this->getResponseStatus($oResponse);
            }
        }
        if (empty($srid)) {
            // check
            $oResponse = Response::create($this->surveyId);
            // startlanguage
            $oResponse->startlanguage = $this->language;
            if (Survey::model()->findByPk($this->surveyId)->datestamp == "Y") {
                $now = dateShift(
                    (string) date("Y-m-d H:i:s"),
                    "Y-m-d H:i:s",
                    Yii::app()->getConfig("timeadjust")
                );
                $oResponse->datestamp = $now;
                $oResponse->startdate = $now;
            }
            foreach ($this->restrictedTo as $column => $value) {
                $oResponse->setAttribute($column, $value);
            }
            if (is_null($this->token)) {
                $oResponse->setAttribute("token", $this->token);
            }
            if (!empty($newSrid)) {
                self::setIdentityInsertResponse($this->surveyId);
                $oResponse->id = $newSrid;
            }
            $oResponse->save();
            $srid = $oResponse->id;
            if (!empty($newSrid)) {
                self::unsetIdentityInsertResponse($this->surveyId);
            }
        }
        /* Save current values */
        $columns = $this->getColumns(true);
        foreach ($answers as $attribute => $answer) {
            $column = $columns[$attribute];
            if (!empty($column["readOnly"]) && $responseState != "readonly") {
                continue;
            }
            switch ($column["type"]) {
                case "numeric":
                    $answer = filter_var(
                        $answer,
                        FILTER_SANITIZE_NUMBER_FLOAT,
                        FILTER_FLAG_ALLOW_FRACTION
                    );
                    if ($answer === "") {
                        $answer = null;
                    } else {
                        $answer = floatval($answer);
                        if (!empty($column["mask"]) && $column["mask"] == "#") {
                            $answer = intval($answer);
                        }
                    }
                    break;
                case "checkbox":
                    $answer = $answer && $answer != "false" ? "Y" : "N";
                    break;
                case "calendar":
                    // "YYYY-MM-DD+HH:ii"
                    if (empty($answer) || strlen($answer) < 10) {
                        $answer = null;
                    } else {
                        $answer = substr($answer, 0, 10); // TODO : check for HH:ii
                        $d = \DateTime::createFromFormat("Y-m-d", $answer);
                        if (!$d || $d->format("Y-m-d") != $answer) {
                            $answer = null;
                        }
                    }
                    break;
                case "dropdown":
                    if (empty($column["url"])) {
                        $sourceIds = array_map(function ($source) {
                            return $source["id"];
                        }, $column["source"]);
                        if ($answer && !in_array($answer, $sourceIds)) {
                            $fixed = false;
                            foreach ($column["source"] as $aSource) {
                                if ($answer == $aSource["name"]) {
                                    $fixed = true;
                                    $answer = $aSource["id"];
                                    break;
                                }
                            }
                            if (!$fixed) {
                                $answer = null;
                            }
                        }
                    } else {
                        $answer = strval($answer);
                        if (strpos($answer, '-')) {
                            $sidsrid = explode("-", $answer);
                            $setsid = intval($sidsrid[0]);
                            $setsrid = intval($sidsrid[1]);
                            $answer = "{$setsid}-{$setsrid}";;
                        } else {
                            $setsid = $answer = intval($answer);
                            $setsrid = null;
                        }
                        if (isset($this->autocompleteUpdates['sid'])) {
                            $oResponse->setAttribute($this->autocompleteUpdates['sid'], $setsid);
                        }
                        if (!is_null($setsrid) && isset($this->autocompleteUpdates['srid'])) {
                            $oResponse->setAttribute($this->autocompleteUpdates['srid'], $setsrid);
                        }
                    }
                default:
                // nothing to do
            }
            $oResponse->setAttribute($attribute, $answer);
        }
        $oResponse->submitdate = null;
        foreach ($this->restrictedTo as $column => $value) {
            $oResponse->setAttribute($column, $value);
        }
        if (!is_null($this->token)) {
            $oResponse->setAttribute("token", $this->token);
        }
        $oResponse->save();
        $this->ownStartSurvey($oResponse);

        /**/
        $aReturn["status"] = $this->ownLoadAndSubmitSurvey($oResponse);
        $oResponse = Response::model($this->surveyId)->findByPk($srid);
        if (empty($oResponse)) {
            // Throw error
            throw new CHttpException(400, gT("Invalid response id."));
        }
        $aReturn["response"] = $this->getResponseData($oResponse);
        $aReturn["responseAttributes"] = $oResponse->getAttributes();
        $aReturn["submitted"] = $oResponse->getAttribute("submitdate");
        return $aReturn;
    }

    /**
     * Wipe all session variables of current survey
     * @deprecated
     * @return void
     */
    private function ownResetSession()
    {
        /* $_SESSION['survey_'.$surveyid]['s_lang']  or App()->getLanguage() ? */
        killSurveySession($this->surveyId);
        unset($_SESSION["fieldmap-" . $this->surveyId . $this->language]);
        \LimeExpressionManager::SetSurveyId($this->surveyId);
        \LimeExpressionManager::SetEMLanguage($this->language);
        SetSurveyLanguage($this->surveyId, $this->language);
        unset($_SESSION["LEMforceRefresh"]);
        //~ unset($_GET['newtest']);
    }

    /**
     * Start survey session like LS core
     * @param \Response::model $oResponses
     * @return void
     */
    private function ownStartSurvey($oResponses)
    {
        killSurveySession($this->surveyId);
        /* What to do when response already exist ? */
        \LimeExpressionManager::SetSurveyId($this->surveyId);
        \LimeExpressionManager::SetEMLanguage($this->language);
        $_SESSION["survey_" . $this->surveyId] = [
            "s_lang" => $this->language,
            "srid" => $oResponses->id,
        ];
        /* Replace buildsurveysession due to usage of App()->getController()->sTemplate , and try to ligthen */
        /* buildsurveysession in frontend_helper */
        $Survey = Survey::model()->findByPk($this->surveyId);
        /* resetAllSessionVariables part in frontend_helper not needed (used only when survey was updated) */
        /* Start to fill the session buildsurveysession at 2022-09-21 version */
        UpdateGroupList($this->surveyId, $this->language);
        $_SESSION["survey_" . $this->surveyId]["totalquestions"] =
            $Survey->countInputQuestions;
        /* Forced survey to be in group by group */
        $_SESSION["survey_" . $this->surveyId]["totalsteps"] = count(
            $_SESSION["survey_" . $this->surveyId]["grouplist"]
        );
        /* The token ? */
        if ($this->token) {
            $_SESSION["survey_" . $this->surveyId]["token"] = $this->token;
        }
        if ($Survey->anonymized == "N") {
            $_SESSION["survey_" . $this->surveyId]["insertarray"][] = "token";
        }
        $fieldmap = $_SESSION["survey_" . $this->surveyId][
            "fieldmap"
        ] = createFieldMap(
            $Survey,
            "full",
            true,
            false,
            $_SESSION["survey_" . $this->surveyId]["s_lang"]
        );
        initFieldArray($this->surveyId, $fieldmap);
        prefillFromCommandLine($this->surveyId);
        if (isset($_SESSION["survey_" . $this->surveyId]["fieldarray"])) {
            $_SESSION["survey_" . $this->surveyId]["fieldarray"] = array_values(
                $_SESSION["survey_" . $this->surveyId]["fieldarray"]
            );
        }
        /* End to fill the session buildsurveysession at 2022-09-21 version */
    }

    /**
     * Start survey session like LS core
     * @param \Response::model $oResponses
     * @return array
     */
    private function ownLoadAndSubmitSurvey($oResponses)
    {
        $this->ownLoadAnswers($oResponses);
        $_SESSION["survey_" . $this->surveyId]["step"] = 0;
        /* Start the survey as group survey */
        \LimeExpressionManager::StartSurvey(
            $this->surveyId,
            "group",
            $this->ownGetSurveyOption(),
            false
        );
        \LimeExpressionManager::JumpTo(0, false, false, true); // no preview, no post and force
        /* Go to count of all groups +1 */
        $numberOfGroups = 1;
        if (!empty($_SESSION["survey_" . $this->surveyId]["totalsteps"])) {
            $numberOfGroups =
                $_SESSION["survey_" . $this->surveyId]["totalsteps"];
        }
        $aMoveResult = \LimeExpressionManager::JumpTo(
            $numberOfGroups + 1,
            false,
            false,
            true
        ); // Force if equation in last group before redo the survey again
        \LimeExpressionManager::JumpTo(0, false, false, true); // no preview, no post and force
        Response::model($this->surveyId)->updateByPk($oResponses->id, [
            "submitdate" => null,
        ]);
        $aMoveResult = \LimeExpressionManager::JumpTo(
            $numberOfGroups + 1,
            false,
            false,
            false
        );
        return $aMoveResult;
    }
    private function ownGetSurveyOption()
    {
        $oSurvey = Survey::model()->findByPk($this->surveyId);
        return [
            "active" => $oSurvey->active == "Y",
            "allowsave" => true,
            "anonymized" => $oSurvey->anonymized != "N",
            "assessments" => false,
            "datestamp" => $oSurvey->datestamp == "Y",
            "deletenonvalues" => Yii::app()->getConfig("deletenonvalues"),
            "hyperlinkSyntaxHighlighting" => false, // TODO set this to true if in admin mode but not if running a survey
            "ipaddr" => $oSurvey->ipaddr == "Y",
            "radix" => ".",
            "refurl" => null,
            "savetimings" => false,
            "surveyls_dateformat" => 1,
            "startlanguage" => $this->language,
            "target" =>
                Yii::app()->getConfig("uploaddir") .
                DIRECTORY_SEPARATOR .
                "surveys" .
                DIRECTORY_SEPARATOR .
                $this->surveyId .
                DIRECTORY_SEPARATOR .
                "files" .
                DIRECTORY_SEPARATOR,
            "tempdir" => Yii::app()->getConfig("tempdir") . DIRECTORY_SEPARATOR,
            "timeadjust" => Yii::app()->getConfig("timeadjust"),
            "token" => $this->token,
        ];
    }

    /**
     * Load answers in current $_SESSION
     * @param \Response::model()
     * @return void
     */
    private function ownLoadAnswers($oResponses)
    {
        $surveyId = $this->surveyId;
        $insertarray = $_SESSION["survey_" . $surveyId]["insertarray"];
        $fieldmap = $_SESSION["survey_" . $surveyId]["fieldmap"];
        foreach ($oResponses->getAttributes() as $column => $value) {
            switch ($column) {
                case "datestamp":
                case "startdate":
                case "startlanguage":
                case "token": // ?
                    $_SESSION["survey_" . $surveyId][$column] = $value;
                    break;
                default:
                    if (
                        in_array($column, $insertarray) &&
                        isset($fieldmap[$column])
                    ) {
                        if (
                            in_array($fieldmap[$column]["type"], [
                                "N",
                                "K",
                                "D",
                            ])
                        ) {
                            if ($value === null) {
                                $value = "";
                            }
                        }
                        $_SESSION["survey_" . $surveyId][$column] = $value;
                    }
            }
        }
    }

    /**
     * return fixed (and filtered) data
     * @param \Respone::model
     * @return array
     */
    public function getResponseData($oResponse)
    {
        foreach ($this->getColumns() as $column) {
            $id = $column["id"];
            if ($column["id"] == "state") {
                $value = $this->getResponseStatus($oResponse);
            } else {
                $value = $oResponse->getAttribute($column["id"]);
            }
            if ($column["type"] == "numeric") {
                if ($value && $value[0] == ".") {
                    $value = "0" . $value;
                }
                if (strpos($value, ".")) {
                    $value = rtrim(rtrim($value, "0"), ".");
                }
            }
            if ($column["type"] == "checkbox") {
                $value = $value == "Y";
            }
            $aResponse[$column["id"]] = $value;
        }
        return $aResponse;
    }

    /**
     * Get the column status
     * @param \Response::model
     * @return string (readonly currently)
     */
    private function getResponseStatus($oResponse)
    {
        foreach ($this->readonlyConditions as $column => $readonlyCondition) {
            $op = $readonlyCondition['op'];
            $value = $readonlyCondition['value'];
            if (
                !empty($oResponse->getAttribute($column))
                && self::criteriaMet($oResponse->getAttribute($column), $op, $value)
            ) {
                return "readonly";
            }
        }
        if (!empty($oResponse->getAttribute('submitdate'))) {
            foreach ($this->readonlySubmittedConditions as $column => $readonlyCondition) {
                $op = $readonlyCondition['op'];
                $value = $readonlyCondition['value'];
                if (
                    !empty($oResponse->getAttribute($column))
                    && self::criteriaMet($oResponse->getAttribute($column), $op, $value)
                ) {
                    return "readonly";
                }
            }
        }
        return "";
    }

    /**
     * get the column sqg
     * @param string
     * @return null|string
     */
    private function getColumnSgq($title)
    {
        $availableColumns = SurveyDynamic::model(
            $this->surveyId
        )->getAttributes(); // in static ?
        /* For token , submitdate etc .... */
        if (array_key_exists($title, $availableColumns)) {
            return $title;
        }
        /* Usage of getQuextionCode OR restrict to single element */
        $oQuestion = Question::model()->find("sid = :sid and title = :title and parent_qid = 0", [
            ":sid" => $this->surveyId,
            ":title" => $title,
        ]);
        if (!$oQuestion) {
            \Yii::log("Invalid question " . $title . " for survey " . $this->surveyId, 'warning', 'plugin.questionSpreadsheetSurvey.dataHelper');
            return null;
        }
        return $oQuestion->sid . "X" . $oQuestion->gid . "X" . $oQuestion->qid;
    }

    /**
     * Set identity insert ON for mssql
     * @param integer $sureyId
     * @return void
     */
    private static function setIdentityInsertResponse($surveyId)
    {
        if (
            !in_array(Yii::app()->db->getDriverName(), [
                "mssql",
                "sqlsrv",
                "dblib",
            ])
        ) {
            return;
        }
        // This needs to be done directly on the PDO object because when using CdbCommand or similar it won't have any effect
        App()->db->pdoInstance->exec(
            "SET IDENTITY_INSERT " .
                App()->db->tablePrefix .
                "survey_" .
                $surveyId .
                " ON"
        );
    }
    /**
     * Set identity insert OFF for mssql
     * @param integer $sureyId
     * @return void
     */
    private static function unsetIdentityInsertResponse($surveyId)
    {
        if (
            !in_array(Yii::app()->db->getDriverName(), [
                "mssql",
                "sqlsrv",
                "dblib",
            ])
        ) {
            return;
        }
        App()->db->pdoInstance->exec(
            "SET IDENTITY_INSERT " .
                App()->db->tablePrefix .
                "survey_" .
                $surveyId .
                " OFF"
        );
    }

    /**
     * Criteria checker
     *
     * @param string $value1 - the value to be compared
     * @param string $operator - the operator
     * @param string $value2 - the value to test against
     * @return boolean - criteria met/not met
     */
    private static function criteriaMet($value1, $operator, $value2)
    {
        switch ($operator) {
            case '<':
                return $value1 < $value2;
            case '<=':
                return $value1 <= $value2;
            case '>':
                return $value1 > $value2;
            case '>=':
                return $value1 >= $value2;
            case '==':
                return $value1 == $value2;
            case '!=':
                return $value1 != $value2;
            default:
                return $value1 == $value2;
        }
    }
}
