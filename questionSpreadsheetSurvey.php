<?php

/**
 * questionSpreadsheetSurvey use a question to add survey inside survey visually like a spreadsheet
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2020-2024 Denis Chenu <www.sondages.pro>
 * @copyright 2020-2024 OECD (Organisation for Economic Co-operation and Development ) <www.oecd.org>
 * @license AGPL v3
 * @version 2.2.1
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

class questionSpreadsheetSurvey extends PluginBase
{
    protected static $name = "questionSpreadsheetSurvey";
    protected static $description = "Add survey inside survey and show it like a spreadsheet.";

    protected $storage = "DbStorage";

    /**
     * @var array[] the settings
     */
    protected $settings = [
        "usageList" => [
            "type" => "info",
            "content" => "Minimum rights are survey read",
        ],
    ];

    /* @var integer : the previous survey value, used when try to savec question attribute */
    private $previousSurvey = "";

    /* @var integer : the current survey id for expression :  to reset after return json data */
    private $currentSurveyId = "";

    /**
     * Add function to be used in beforeQuestionRender event and to attriubute
     */
    public function init()
    {
        Yii::setPathOfAlias("questionSpreadsheetSurvey", dirname(__FILE__));

        $this->subscribe("beforeSurveyPage");

        $this->subscribe("beforeQuestionRender");
        $this->subscribe(
            "newQuestionAttributes",
            "addSpreadsheetSurveyAttribute"
        );

        $this->subscribe("beforeSurveyPage");

        $this->subscribe("newDirectRequest");
        /* Check if user have sufficient right on related survey */
        $this->subscribe("beforeQuestionAttributeSave");
        if (intval(App()->getConfig("versionnumber")) <= 3) {
            /* We are in 3.X and lesser : attribute was deleted before save again */
            $this->subscribe("beforeControllerAction");
        }
    }

    /**
     * Add the list of used survey link
     * @see parent:getPluginSettings
     */
    public function getPluginSettings($getValues = true)
    {
        $pluginSettings = parent::getPluginSettings($getValues);
        if (
            !App()->getController() ||
            App()
                ->getController()
                ->getId() != "admin"
        ) {
            return $pluginSettings;
        }
        if (
            !App()
                ->getController()
                ->getAction() ||
            App()
                ->getController()
                ->getAction()
                ->getId() != "pluginmanager"
        ) {
            return $pluginSettings;
        }
        if (Yii::app()->request->getParam("sa") != "configure") {
            return $pluginSettings;
        }
        if (!Permission::model()->hasGlobalPermission("surveys")) {
            return $pluginSettings;
        }
        $dataViews = [];
        $oQuestionExtraSurveys = QuestionAttribute::model()->findAll([
            "condition" =>
                "attribute = :attribute and (value is not null and value <> '')",
            "params" => [":attribute" => "spreadsheetSurvey"],
            "order" => "value ASC",
        ]);
        $aQidSurveys = [];
        foreach ($oQuestionExtraSurveys as $oQuestionExtraSurvey) {
            $data = [
                "qid" => $oQuestionExtraSurvey->qid,
                "gid" => null,
                "sid" => null,
                "sidname" => null,
                "withsid" => trim($oQuestionExtraSurvey->value),
                "withsidname" => null,
            ];
            $oQuestion = Question::model()->find("qid = :qid", [
                ":qid" => $oQuestionExtraSurvey->qid,
            ]);
            if ($oQuestion) {
                $data["sid"] = $oQuestion->sid;
                $data["gid"] = $oQuestion->gid;
                $oSurveyQid = Survey::model()->findByPk($oQuestion->sid);
                if ($oSurveyQid) {
                    $data["sidname"] = $oSurveyQid->getLocalizedTitle();
                }
            }
            $oSurveyRelated = Survey::model()->findByPk(
                trim($oQuestionExtraSurvey->value)
            );
            if ($oSurveyRelated) {
                $data["withsidname"] = $oSurveyRelated->getLocalizedTitle();
            }
            $aQidSurveys[$oQuestionExtraSurvey->qid] = $data;
        }
        $dataViews["aQidSurveys"] = $aQidSurveys;
        $dataViews["surveylink"] = ["surveyAdministration/view"];
        $dataViews["questionlink"] = ["questionAdministration/view"];
        if (App()->getConfig("versionnumber") < 4) {
            $dataViews["surveylink"] = ["admin/survey/sa/view"];
            $dataViews["questionlink"] = ["admin/questions/sa/view"];
        }
        $pluginSettings["usageList"]["content"] = $this->renderPartial(
            "admin.usage",
            $dataViews,
            true
        );
        return $pluginSettings;
    }

    /**
     * The attributes to manage SpreadsheetSurvey
     */
    public function addSpreadsheetSurveyAttribute()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $questionAttributeEvent = $this->getEvent();
        $extraAttributes = [
            "spreadsheetSurvey" => [
                "types" => "XT",
                "category" => $this->translate("Spreadsheet survey"),
                "sortorder" => 10 /* Own category */,
                "inputtype" => "integer",
                "min" => 0,
                "default" => "",
                "help" => $this->translate("Use an integer"),
                "caption" => $this->translate("Spreadsheet survey id"),
            ],
            "spreadsheetSurveyFillAnswer" => [
                "types" => "T",
                "category" => $this->translate("Spreadsheet survey"),
                "sortorder" => 15 /* Own category */,
                "inputtype" => "singleselect",
                "options" => [
                    "listall" => $this->translate("List of all answers."),
                    "listsubmitted" => $this->translate(
                        "List of submitted answers."
                    ),
                    "number" => $this->translate(
                        "Number of submitted and not submitted answers."
                    ),
                ],
                "default" => "number",
                "help" => $this->translate(
                    "Recommended method is number : submitted answer as set as integer part, and not submitted as decimal part (<code>submitted[.not-submitted]</code>).You can check if all answer are submitted with <code>intval(self)==self</code>."
                ),
                "caption" => $this->translate("Way for filling this question."),
            ],
            "spreadsheetSurveyQuestionLink" => [
                "types" => "XT",
                "category" => $this->translate("Spreadsheet survey"),
                "sortorder" => 20 /* Own category */,
                "inputtype" => "text",
                "default" => "",
                "help" => $this->translate(
                    "The question code in the extra survey to be used. If empty : only token or optionnal fields was used for the link."
                ),
                "caption" => $this->translate("Question for response id"),
            ],
            "spreadsheetSurveyOtherField" => [
                "types" => "XT",
                "category" => $this->translate("Spreadsheet survey"),
                "sortorder" => 30 /* Own category */,
                "inputtype" => "textarea",
                "default" => "",
                "expression" => 1,
                "help" => $this->translate(
                    "One field by line, field must be a valid question code (single question only). Field and value are separated by colon (<code>:</code>), you can use Expression Manager in value."
                ),
                "caption" => $this->translate(
                    "Other question fields for relation."
                ),
            ],
            "spreadsheetSurveyTokenUsage" => [
                "types" => "XT",
                "category" => $this->translate("Spreadsheet survey"),
                "sortorder" => 40 /* Own category */,
                "inputtype" => "singleselect",
                "options" => [
                    "no" => gT("No"),
                    "token" => gT("Yes"),
                    "group" => $this->translate(
                        "Token Group (with responseListAndManage plugin)"
                    ),
                ],
                "default" => "token",
                "help" => $this->translate(
                    "can be used only both survey are not anonymous and current survey have a token table."
                ),
                "caption" => $this->translate("Usage of token."),
            ],
            "spreadsheetSurveyOrderBy" => [
                "types" => "XT",
                "category" => $this->translate("Spreadsheet survey"),
                "sortorder" => 50 /* Own category */,
                "inputtype" => "text",
                "default" => "",
                "help" => sprintf(
                    $this->translate(
                        "You can use %sSGQA identifier%s or question code iof you have getQuestionInformation plugin for the columns to be ordered. The default order is ASC, you can use DESC. You can use <code>,</code> for multiple order."
                    ),
                    '<a href="https://manual.limesurvey.org/SGQA_identifier" target="_blank">',
                    "</a>"
                ),
                "caption" => $this->translate(
                    "Default order by (default “id DESC”, “datestamp ASC” for datestamped surveys)"
                ),
            ],
            "spreadsheetSurveyForcedFillQuestion" => [
                "types" => "T",
                "category" => $this->translate("Spreadsheet survey"),
                "sortorder" => 100 /* Own category */,
                "inputtype" => "text",
                "default" => "",
                "help" => $this->translate(
                    "The question code in the extra survey to be used for prefilling."
                ),
                "caption" => $this->translate("Question to prefill"),
            ],
            "spreadsheetSurveyForcedFillValues" => [
                "types" => "XT",
                "category" => $this->translate("Spreadsheet survey"),
                "sortorder" => 110 /* Own category */,
                "inputtype" => "textarea",
                "default" => "",
                "expression" => 1,
                "help" => $this->translate(
                    "The values to be prefilled, can use Expression manager. If set this disable adding new lines."
                ),
                "caption" => $this->translate("Values to prefill"),
            ],
            "spreadsheetSurveyForcedSubmitted" => [
                "types" => "XT",
                "category" => $this->translate("Spreadsheet survey"),
                "sortorder" => 120 /* Own category */,
                "inputtype" => "switch",
                "options" => [
                    "0" => gT("No"),
                    "forced" => gT("Yes"),
                ],
                "default" => "0",
                "help" => $this->translate(
                    "When the reponse for forced value will be created, the related survey as set as submitted, no contraol is done."
                ),
                "caption" => $this->translate(
                    "Set created survey as submitted"
                ),
            ],
            "spreadsheetSurveyDeleteNotForcedValues" => [
                "types" => "XT",
                "category" => $this->translate("Spreadsheet survey"),
                "sortorder" => 150 /* Own category */,
                "inputtype" => "switch",
                "options" => [
                    "0" => gT("No"),
                    "1" => gT("Yes"),
                ],
                "default" => "1",
                "help" => $this->translate(
                    "If force fill values is set you can delete extra values, deleted value use existing relations. If not : you need to use an extra (hidden) question to show only this question."
                ),
                "caption" => $this->translate("Remove extra values"),
            ],
            "spreadsheetSurveyFixedLines" => [
                "types" => "XT",
                "category" => $this->translate("Spreadsheet survey"),
                "sortorder" => 160 /* Own category */,
                "inputtype" => "switch",
                "options" => [
                    "0" => gT("No"),
                    "1" => gT("Yes"),
                ],
                "default" => "0",
                "help" => $this->translate(
                    "This disable adding or remove response in related survey, you must prefill using another solution. The previous setting force this and give you a way to have response."
                ),
                "caption" => $this->translate("Fixed lines"),
            ],
            "spreadsheetSurveyNoDelete" => [
                "types" => "XT",
                "category" => $this->translate("Spreadsheet survey"),
                "sortorder" => 165 /* Own category */,
                "inputtype" => "switch",
                "options" => [
                    "0" => gT("No"),
                    "1" => gT("Yes"),
                ],
                "default" => "0",
                "help" => $this->translate(
                    "This disable remove response in related survey with right click here, remind to disable delete in reklated survey if needed too."
                ),
                "caption" => $this->translate("Disable delete lines"),
            ],
            "spreadsheetSurveyHiddenQuestions" => [
                "types" => "XT",
                "category" => $this->translate("Spreadsheet survey"),
                "sortorder" => 170 /* Own category */,
                "inputtype" => "textarea",
                "default" => "",
                "expression" => 1,
                "help" => $this->translate(
                    "To hide question, you can use hidden attribute, but if needed you can hide it here too. Use <code>,</code> as separator."
                ),
                "caption" => $this->translate("Question to be hidden"),
            ],
            "spreadsheetSurveyReadOnlyCondition" => [
                "types" => "XT",
                "category" => $this->translate("Spreadsheet survey"),
                "sortorder" => 200 /* Own category */,
                "inputtype" => "textarea",
                "default" => "",
                "expression" => 1,
                "help" => $this->translate(
                    "Use condition to fix some question as readonly. One field by line, field must be a valid question code (single question only). Field and value are separated by colon (<code>:</code>), you can use Expression Manager in value. The line are shown as readonly when load the initial data. Only one condition muts be set to set the line as readonly. Response not submitted are never set to read only."
                ),
                "caption" => $this->translate("Condition for readonly line"),
            ],
            "spreadsheetSurveyReadOnlyConditionSubmitted" => [
                "types" => "XT",
                "category" => $this->translate("Spreadsheet survey"),
                "sortorder" => 205 /* Own category */,
                "inputtype" => "switch",
                "options" => [
                    "0" => gT("No"),
                    "1" => gT("Yes"),
                ],
                "default" => "1",
                "help" => $this->translate(
                    "Readonly line only for data submitted."
                ),
                "caption" => $this->translate("Readonly line only for submitted data"),
            ],
            //~ 'spreadsheetSurveyNumberOfLines' => array(
            //~ 'types' => 'T',
            //~ 'category' => $this->translate('Spreadsheet survey'),
            //~ 'sortorder' =>210, /* Own category */
            //~ 'inputtype' => 'text',
            //~ 'default' => '',
            //~ 'expression' =>1,
            //~ 'help' => $this->translate('Maximum number of lines, can use Expression manager.'),
            //~ 'caption' => $this->translate('Maximum number of lines'),
            //~ ),
            /* Attribute for related question (inside spreadsheet surey */
            "relatedSpreadsheetResponseComplete" => [
                "types" => "S",
                "category" => $this->translate("Spreadsheet survey responses dropdown"),
                "sortorder" => 10 /* Own category */,
                "inputtype" => "text",
                "default" => "",
                "help" => $this->translate("This allow to use surveys response for autocomplete in spreadSheetSurvey. Workiung only for spreadshit survey, not for simple survey."),
                "caption" => $this->translate("Related survey(s) for autocomplete. WIP: return all value without autocomplete"),
            ],
            "relatedSpreadsheetResponseCompleteTokenUsage" => [
                "types" => "S",
                "category" => $this->translate("Spreadsheet survey responses dropdown"),
                "sortorder" => 20, /* Own category */
                "inputtype" => "singleselect",
                "options" => [
                    "no" => gT("No"),
                    "token" => gT("Yes"),
                    "group" => gT("Token Group (with responseListAndManage plugin)")
                ],
                "default" => "token",
                "help" => $this->gT("If you have responseListAndManage, the response list can be found using the group of current token. If the related survey have a token table : token stay mandatory."),
                "caption" => $this->gT("Usage of token."),
            ],
            "relatedSpreadsheetResponseCompleteQuestion" => [
                "types" => "S",
                "category" => $this->gT("Spreadsheet survey responses dropdown"),
                "sortorder" => 30, /* Own category */
                "inputtype" => "text",
                "default" => "",
                "help" => $this->gT("This can be text question type, single choice question type or equation question type."),
                "caption" => $this->gT("The question code used for the text for the label."),
            ],
            "relatedSpreadsheetResponseCompleteCategories" => [
                "types" => "S",
                "category" => $this->gT("Spreadsheet survey responses dropdown"),
                "sortorder" => 40,
                "inputtype" => "textarea",
                "default" => "",
                "i18n" => true,
                "help" => $this->gT("If there are multiple surveys, you can have a different categorie for each. One line by value, Survey ID and categorie strong separated by colon (“:“)."),
                "caption" => $this->gT("Optionnal categories for multiple surveys "),
            ],
            "relatedSpreadsheetResponseCompleteSID" => [
                "types" => "S",
                "category" => $this->gT("Spreadsheet survey responses dropdown"),
                "sortorder" => 50,
                "inputtype" => "text",
                "default" => "",
                "i18n" => false,
                "help" => $this->gT("Put the title of a single question type (short text or numerical for exemple) in this survey to get directly the survey id in data."),
                "caption" => $this->gT("Optionnal question title for survey ID."),
            ],
            "relatedSpreadsheetResponseCompleteSRID" => [
                "types" => "S",
                "category" => $this->gT("Spreadsheet survey responses dropdown"),
                "sortorder" => 51,
                "inputtype" => "text",
                "default" => "",
                "i18n" => false,
                "help" => $this->gT("Put the title of a single question type (short text or numerical for exemple) in this survey to get directly the response id in data."),
                "caption" => $this->gT("Optionnal question title for response ID."),
            ],
            //~ "relatedSpreadsheetResponseLimit" => [
                //~ "types" => "S",
                //~ "category" => $this->gT("Spreadsheet survey responses dropdown"),
                //~ "sortorder" => 100,
                //~ "inputtype" => "integere",
                //~ "min" => 0,
                //~ "default" => 10,
                //~ "help" => $this->gT("Set the mlimit of number of response returen when search value. The limit are for each survey."),
                //~ "caption" => $this->gT("Limit of reponse return when search"),
            //~ ],
        ];
        if (method_exists($questionAttributeEvent, "append")) {
            $questionAttributeEvent->append(
                "questionAttributes",
                $extraAttributes
            );
        } else {
            $questionAttributes = (array) $questionAttributeEvent->get(
                "questionAttributes"
            );
            $questionAttributes = array_merge(
                $questionAttributes,
                $extraAttributes
            );
            $questionAttributeEvent->set(
                "questionAttributes",
                $questionAttributes
            );
        }
    }

    /**
     * get previous survey value in 3.X
     */
    public function beforeControllerAction()
    {
        $controllerActionEvent = $this->getEvent();
        if ($controllerActionEvent->get("controller") != "admin") {
            return;
        }
        if ($controllerActionEvent->get("action") != "database") {
            return;
        }
        if (
            empty(
                App()
                    ->getRequest()
                    ->getpost("spreadsheetSurvey")
            )
        ) {
            return;
        }
        $qid = App()
            ->getRequest()
            ->getpost("qid");
        if (empty($qid)) {
            return;
        }
        $oExtraSurvey = QuestionAttribute::model()->find(
            "qid = :qid and attribute = :attribute",
            [":qid" => $qid, ":attribute" => "spreadsheetSurvey"]
        );
        if (empty($oExtraSurvey)) {
            return;
        }
        $this->previousSurvey = $oExtraSurvey->value;
    }

    /**
     * Control surveyid when save
     * @todo : add relatedSpreadsheetResponseComplete
     */
    public function beforeQuestionAttributeSave()
    {
        if (Yii::app() instanceof CConsoleApplication) {
            return;
        }
        if (Permission::model()->hasGlobalPermission("surveys", "update")) {
            return;
        }
        $model = $this->getEvent()->get("model");
        $attribute = $model->getAttribute("attribute");
        if ($attribute != "spreadsheetSurvey" || $attribute != "relatedSpreadsheetResponseComplete") {
            return;
        }
        $model->setAttribute("value", trim($model->getAttribute("value")));
        if (empty($model->getAttribute("value"))) {
            return;
        }
        $sid = $model->getAttribute("value");
        /* Find if same than previous*/
        $previous = $this->previousSurvey;
        if ($model->getAttribute("qaid")) {
            $previousModel = QuestionAttribute::model()->findByPk(
                $model->getAttribute("qaid")
            );
            if ($previousModel) {
                $previous = trim($previousModel->getAttribute("value"));
            }
        }
        if ($previous == $model->getAttribute("value")) {
            return;
        }
        switch ($attribute) {
            case "spreadsheetSurvey":
                if (
                    Permission::model()->hasSurveyPermission(
                        $sid,
                        "responses",
                        "update"
                    )
                ) {
                    return;
                }
                $model->setAttribute("value", $previous);
                App()->setFlashMessage(
                    sprintf(gT("You do not have sufficient right on survey %s"), $sid),
                    "warning"
                );
                break;
            case "relatedSpreadsheetResponseComplete":
                /* explode */
                $surveysId = explode(",", $model->getAttribute("value"));
                $surveysId = array_filter(array_map(function ($surveyId) {
                    $surveyId = intval($surveyId);
                    if ($surveyId && Permission::model()->hasSurveyPermission($surveyId, "responses", "read")) {
                        return $surveyId;
                    }
                    if ($surveyId) {
                        App()->setFlashMessage(
                            sprintf(gT("You do not have sufficient right on survey %s"), $surveyId),
                            "warning"
                        );
                    }
                }, $surveysId));
                /* have permission */
                if (!empty($surveysId)) {
                    $model->setAttribute("value", implode($surveysId));
                } else {
                    $model->setAttribute("value", $previous);
                }
                break;
        }
    }

    /**
     * Access control on survey
     */
    public function beforeSurveyPage()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $iSurveyId = $this->event->get("surveyId");
    }

    /**
     * Add the script when question is rendered
     * Add QID and SGQ replacement forced (because it's before this was added by core
     */
    public function beforeQuestionRender()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $renderEvent = $this->getEvent();
        $aQuestionAttributes = QuestionAttribute::model()->getQuestionAttributes(
            $renderEvent->get("qid"),
            Yii::app()->getLanguage()
        );
        $surveyId = $renderEvent->get("surveyId");
        if (
            empty($aQuestionAttributes["spreadsheetSurvey"]) ||
            trim($aQuestionAttributes["spreadsheetSurvey"]) == ""
        ) {
            return;
        }
        $token = Yii::app()
            ->getRequest()
            ->getParam("token");
        if (empty($token)) {
            $token = !empty(Yii::app()->session["survey_$surveyId"]["token"])
                ? Yii::app()->session["survey_$surveyId"]["token"]
                : null;
        }
        $thisSurvey = Survey::model()->findByPk($surveyId);
        $extraSurveyAttribute = trim($aQuestionAttributes["spreadsheetSurvey"]);
        $extraSurveyTokenUsage = trim($aQuestionAttributes["spreadsheetSurveyTokenUsage"]);

        if (!ctype_digit($extraSurveyAttribute)) {
            $oLangSurvey = SurveyLanguageSetting::model()->find([
                "select" => "surveyls_survey_id",
                "condition" =>
                    "surveyls_title = :title AND surveyls_language =:language ",
                "params" => [
                    ":title" => $extraSurveyAttribute,
                    ":language" => Yii::app()->getLanguage(),
                ],
            ]);
            if (!$oLangSurvey) {
                return;
            }
            $extraSurveyAttribute = $oLangSurvey->surveyls_survey_id;
        }

        $extraSurvey = Survey::model()->findByPk($extraSurveyAttribute);
        $disableMessage = "";
        if (!$extraSurvey) {
            $disableMessage = sprintf(
                $this->translate("Invalid survey %s for question %s."),
                $extraSurveyAttribute,
                $renderEvent->get("qid")
            );
        }
        if (!$disableMessage && $extraSurvey->active != "Y") {
            $disableMessage = sprintf(
                $this->translate("Survey %s for question %s not activated."),
                $extraSurveyAttribute,
                $renderEvent->get("qid")
            );
        }
        if (
            !$disableMessage &&
            !$this->accessWithToken($thisSurvey) &&
            $extraSurveyTokenUsage != "no" &&
            $this->accessWithToken($extraSurvey)
        ) {
            $disableMessage = sprintf(
                $this->translate(
                    "Survey %s for question %s can not be used with a survey without tokens."
                ),
                $extraSurveyAttribute,
                $renderEvent->get("qid")
            );
        }
        if (
            !$disableMessage &&
            $this->accessWithToken($thisSurvey) &&
            $extraSurveyTokenUsage != "no" &&
            $extraSurvey->anonymized == "Y"
        ) {
            $disableMessage = sprintf(
                $this->translate(
                    "Survey %s for question %s need to be not anonymized."
                ),
                $extraSurveyAttribute,
                $renderEvent->get("qid")
            );
        }
        if (
            !$disableMessage &&
            $this->accessWithToken($extraSurvey) &&
            $extraSurveyTokenUsage != "no"
        ) {
            if (!$this->validateToken($extraSurvey, $thisSurvey, $token)) {
                $disableMessage = sprintf(
                    $this->translate(
                        "Survey %s for question %s token can not ne found or created."
                    ),
                    $extraSurveyAttribute,
                    $renderEvent->get("qid")
                );
            }
        }

        if ($disableMessage) {
            $renderEvent->set(
                "answers",
                CHtml::tag(
                    "div",
                    ["class" => "alert alert-warning"],
                    $disableMessage
                )
            );
            return;
        }
        $this->setSurveySpreadsheetForAnswer(
            $renderEvent,
            $extraSurvey->sid,
            $aQuestionAttributes,
            $token
        );
    }

    /**
     * Set the answwer and other parameters for the system
     * @param int $surveyId for answers
     * @param array $qAttributes
     * @param string $token
     * @return void
     */
    private function setSurveySpreadsheetForAnswer(
        $renderEvent,
        $surveyId,
        $aQuestionAttributes,
        $token = null
    ) {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $currentSurveyId = $renderEvent->get("surveyId");
        $qid = $renderEvent->get("qid");
        $srid = isset($_SESSION["survey_{$currentSurveyId}"]["srid"])
            ? $_SESSION["survey_{$currentSurveyId}"]["srid"]
            : null;
        /* Unset session : force reeavaluation */
        $this->unsetSession($qid, $srid);
        $renderEvent->set(
            "class",
            $renderEvent->get("class") . " questionSpreadsheetSurvey"
        );
        $aWarnings = [];
        /** Get the questions */
        /** script */
        /* Construction of options **/
        $jexcelOptions = [];
        $this->subscribe("getPluginTwigPath", "addTwigPath");
        /* TODO : move $dataHelper to getDataHelper */
        if (intval(App()->getConfig("versionnumber")) <= 3) {
            $dataHelper = new \questionSpreadsheetSurvey\helpers3LTS\dataHelper(
                $surveyId
            );
        } else {
            $dataHelper = new \questionSpreadsheetSurvey\helpers\dataHelper(
                $surveyId
            );
        }
        $sTokenUsage = trim(
            $this->getQuestionAttribute($qid, "spreadsheetSurveyTokenUsage")
        );
        if ($token) {
            if ($sTokenUsage != "no") {
                $dataHelper->setToken($token);
            }
            if ($sTokenUsage == "group") {
                $dataHelper->tokens = $this->getTokensList(
                    $currentSurveyId,
                    $token
                );
            }
        }
        $spreadsheetSurveyQuestionLink = trim(
            $this->getQuestionAttribute($qid, "spreadsheetSurveyQuestionLink")
        );
        if ($spreadsheetSurveyQuestionLink) {
            if (
                $dataHelper->setColumnsAsReadonly(
                    $spreadsheetSurveyQuestionLink
                )
            ) {
                $bFixedLines = true;
            } else {
                $this->log(
                    "Invalid question for response id : $spreadsheetSurveyQuestionLink in $currentSurveyId for question $qid",
                    \CLogger::LEVEL_WARNING
                );
                $aWarnings[] = sprintf(
                    $this->translate("Invalid question %s for response id"),
                    CHtml::encode($spreadsheetSurveyQuestionLink)
                );
            }
        }
        /* Manual other relation */
        $spreadsheetSurveyOtherField = $this->getQuestionAttribute(
            $qid,
            "spreadsheetSurveyOtherField"
        );
        $aSpreadsheetSurveyOtherField = $this->fixOtherSpreadsheetField(
            $qid,
            $srid,
            $spreadsheetSurveyOtherField
        );
        if ($aSpreadsheetSurveyOtherField) {
            foreach (array_keys($aSpreadsheetSurveyOtherField) as $title) {
                if ($dataHelper->setColumnsAsReadonly($title)) {
                    $bFixedLines = true;
                } else {
                    $this->log(
                        "Invalid question for other field : $title in $currentSurveyId for question $qid",
                        \CLogger::LEVEL_WARNING
                    );
                    $aWarnings[] = sprintf(
                        $this->translate("Invalid question %s for other field"),
                        CHtml::encode($title)
                    );
                }
            }
        }
        $sForcedFillQuestion = trim(
            $this->getQuestionAttribute(
                $qid,
                "spreadsheetSurveyForcedFillQuestion"
            )
        );
        $sForcedFillValues = trim(
            $this->getQuestionAttribute(
                $qid,
                "spreadsheetSurveyForcedFillValues"
            )
        );
        $aForcedFillValues = $this->fixValuePrefillSpreadsheetField(
            $qid,
            $srid,
            $sForcedFillValues
        );
        $bFixedLines = $this->getQuestionAttribute(
            $qid,
            "spreadsheetSurveyFixedLines"
        );
        $bDisableDelete = $this->getQuestionAttribute(
            $qid,
            "spreadsheetSurveyNoDelete"
        );

        if (!empty($sForcedFillQuestion)) {
            if ($dataHelper->setColumnsAsReadonly($sForcedFillQuestion)) {
                $bFixedLines = true;
            } else {
                $this->log(
                    "Invalid question for forced field : $sForcedFillQuestion in $currentSurveyId for question $qid",
                    \CLogger::LEVEL_WARNING
                );
                $aWarnings[] = sprintf(
                    $this->translate("Invalid question %s for forced field"),
                    CHtml::encode($sForcedFillQuestion)
                );
            }
        }
        if ($bFixedLines) {
            $dataHelper->addEmptyLine = false;
            $jexcelOptions["allowDeleteRow"] = false;
            $jexcelOptions["allowDeletingAllRows"] = false;
            $jexcelOptions["allowInsertRow"] = false;
        }
        if ($bDisableDelete) {
            $jexcelOptions["allowDeleteRow"] = false;
            $jexcelOptions["allowDeletingAllRows"] = false;
        }
        $sHiddenQuestions = trim(
            $this->getQuestionAttribute(
                $qid,
                "spreadsheetSurveyHiddenQuestions"
            )
        );
        if (!empty($sHiddenQuestions)) {
            $aHiddenColumns = array_filter(
                preg_split("/\s*(,|;)\s*/", trim($sHiddenQuestions))
            );
            foreach ($aHiddenColumns as $sHiddenColumn) {
                if ($dataHelper->setColumnsAsHidden($sHiddenColumn)) {
                    $bFixedLines = true;
                } else {
                    $this->log(
                        "Invalid question for hidden column : $sHiddenColumn in $currentSurveyId for question $qid",
                        \CLogger::LEVEL_WARNING
                    );
                    $aWarnings[] = sprintf(
                        $this->translate(
                            "Invalid question %s for  for hidden column"
                        ),
                        CHtml::encode($sHiddenColumn)
                    );
                }
            }
        }
        $columns = $dataHelper->getColumns();
        $jexcelOptions["columns"] = $columns;
        $this->registerSpreadsheetPackage();
        $urlDatas = [
            "qid" => $qid,
            "sid" => $currentSurveyId,
            "srid" => $srid,
            "token" => $token,
            "lang" => Yii::app()->getLanguage(),
        ];
        $getDataUrl = Yii::app()
            ->getController()
            ->createUrl(
                "plugins/direct",
                array_merge(
                    ["plugin" => get_class($this), "function" => "getdata"],
                    $urlDatas
                )
            );
        $jexcelOptions["url"] = $getDataUrl;
        $updateDataUrl = Yii::app()
            ->getController()
            ->createUrl(
                "plugins/direct",
                array_merge(
                    ["plugin" => get_class($this), "function" => "update"],
                    $urlDatas
                )
            );
        $deleteDataUrl = Yii::app()
            ->getController()
            ->createUrl(
                "plugins/direct",
                array_merge(
                    ["plugin" => get_class($this), "function" => "delete"],
                    $urlDatas
                )
            );
        $resultUrl = Yii::app()
            ->getController()
            ->createUrl(
                "plugins/direct",
                array_merge(
                    ["plugin" => get_class($this), "function" => "result"],
                    $urlDatas
                )
            );
        $spreadsheetOptions = [
            "jexcel" => $jexcelOptions,
            "updateUrl" => $updateDataUrl,
            "deleteUrl" => $deleteDataUrl,
            "resultUrl" => $resultUrl,
        ];
        $spreadsheetGlobalOptions = [
            "urlPostBase" => [
                Yii::app()->request->csrfTokenName => Yii::app()->request
                    ->csrfToken,
            ],
        ];
        /** view */
        $renderData = [
            "aSurveyInfo" => getSurveyInfo(
                $currentSurveyId,
                App()->getLanguage()
            ),
            "qid" => $qid,
        ];
        $renderData["spreadsheetOptions"] = $spreadsheetOptions;
        $renderData["warnings"] = Permission::model()->hasSurveyPermission(
            $currentSurveyId,
            "surveycontent",
            "update"
        )
            ? $aWarnings
            : [];
        $renderData["inputName"] = $inputName =
            $currentSurveyId .
            "X" .
            $renderEvent->get("gid") .
            "X" .
            $renderEvent->get("qid");
        $renderData["relatedSurvey"] = $surveyId;
        $renderData["token"] = $token;
        $renderData["relationalData"] = [
            "otherFields" => $aSpreadsheetSurveyOtherField,
            "fillValue" => $aForcedFillValues,
        ];
        $renderData["value"] = $this->getResult($qid, $srid, $token, 1);
        $answer = App()->twigRenderer->renderPartial(
            "/survey/questions/answer/spreadsheetsurvey/answer.twig",
            $renderData
        );
        $spreadsheetLanguage = [
            "An unknow error happen, please reload." => implode("\n", [
                $this->translate("An unknow error happen, please reload."),
                gT(
                    "Either you have been inactive for too long, you have cookies disabled for your browser, or there were problems with your connection."
                ),
            ]),
            "The CSRF token could not be verified." => implode("\n", [
                gT("The CSRF token could not be verified."),
                gT(
                    "Either you have been inactive for too long, you have cookies disabled for your browser, or there were problems with your connection."
                ),
            ]),
            "We are sorry but your session has expired." => implode("\n", [
                gT("We are sorry but your session has expired."),
                gT(
                    "Either you have been inactive for too long, you have cookies disabled for your browser, or there were problems with your connection."
                ),
            ]),
        ];
        App()
            ->getClientScript()
            ->registerScript(
                "questionSpreadsheetSurveyGlobalOptions",
                "questionSpreadsheetSurvey.GlobalOptions = " .
                    json_encode($spreadsheetGlobalOptions) .
                    "\n" .
                    "questionSpreadsheetSurvey.language = " .
                    json_encode($spreadsheetLanguage) .
                    "\n"
            );
        $renderEvent->set("answers", $answer);
        $this->unsubscribe("getPluginTwigPath");
    }

    /**
     * @see vent
     * just add twig directory , disable after usage
     */
    public function addTwigPath()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $viewPath = dirname(__FILE__) . "/twig";
        $this->getEvent()->append("add", [$viewPath]);
    }

    /**
     * @see event
     * update according to params and right the related survey
     * @throw Exceptuon
     * @return void
     */
    public function newDirectRequest()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $oEvent = $this->event;
        if ($oEvent->get("target") != get_class()) {
            return;
        }
        $sAction = $oEvent->get("function");
        $lang = $this->api->getRequest()->getParam("lang");
        $qid = $this->api->getRequest()->getParam("qid");
        $questionSid = $this->api->getRequest()->getParam("sid");
        $srid = $this->api->getRequest()->getParam("srid");
        $token = $this->api->getRequest()->getParam("token");
        if ($sAction == "autocomplete" && $qid) {
            $this->getAutocompleteSource($qid, $lang, $token);
        }
        $this->getSidFromQidValidity($qid);
        if (!$this->checkSession($qid, $srid)) {
            /* Session expired ?  */
            $this->throwAdaptedException(
                400,
                gT("We are sorry but your session has expired.") .
                    "\n" .
                    gT(
                        "Either you have been inactive for too long, you have cookies disabled for your browser, or there were problems with your connection."
                    )
            );
        }
        App()->setConfig("deletenonvalues", 1); // https://gitlab.com/SondagesPro/QuestionSettingsType/questionSpreadsheetSurvey/-/issues/4
        $this->currentSurveyId = Yii::app()->session["LEMsid"];
        switch ($sAction) {
            case "delete":
                $this->deleteData($qid, $srid, $token);
                break;
            case "result":
                $this->getResult($qid, $srid, $token);
                break;
            case "update":
                $this->updateData($qid, $srid, $token);
                break;
            case "getdata":
            default:
                // getdata is the default
                $this->getData($qid, $srid, $token);
                break;
        }
    }

    /**
     * Add the packages needed
     * @return void
     */
    private function registerSpreadsheetPackage()
    {
        Yii::app()->clientScript->addPackage("jsuites", [
            "basePath" => "questionSpreadsheetSurvey.vendor.jsuites.dist",
            "js" => ["jsuites.js"],
            "css" => ["jsuites.css"],
        ]);
        Yii::app()->clientScript->addPackage("jexcel", [
            "basePath" => "questionSpreadsheetSurvey.vendor.jexcel.dist",
            "js" => ["jexcel.js"],
            //~ 'css'          => array('jexcel.css'),
            "depends" => ["jsuites"],
        ]);
        Yii::app()->clientScript->addPackage("questionSpreadsheetSurvey", [
            "basePath" => "questionSpreadsheetSurvey.assets",
            "js" => ["questionSpreadsheetSurvey.js"],
            "css" => ["jexcel-light.css", "questionSpreadsheetSurvey.css"],
            "depends" => ["jexcel", "jquery"],
        ]);
        Yii::app()
            ->getClientScript()
            ->registerPackage("questionSpreadsheetSurvey");
    }

    /**
     * get the data for this particular id and return it in a json file
     * @param integer $surveyId
     * @param string $lang
     * @param string $token
     */
    private function getData($qid, $srid, $token, $lang = null)
    {
        if (!$lang) {
            $lang = Yii::app()->getLanguage();
        }
        $dataHelper = $this->getDataHelper($qid, $srid, $token);
        $orderBy = trim(
            $this->getQuestionAttribute($qid, "spreadsheetSurveyOrderBy")
        );
        $dataHelper->setOrderBy($orderBy);
        $data = $dataHelper->getData();
        $this->displayJson($data);
    }

    /**
     * get the data for this particular id and return it in a json file
     * @param integer $qid
     * @param integer $srid
     * @param string $token
     * @param boolean $return
     * @param string $lang
     */
    private function getResult(
        $qid,
        $srid,
        $token,
        $return = false,
        $lang = null
    ) {
        if (!$lang) {
            $lang = Yii::app()->getLanguage();
        }
        $dataHelper = $this->getDataHelper($qid, $srid, $token);
        $result = $dataHelper->getResult(
            $this->getQuestionAttribute(
                $qid,
                "spreadsheetSurveyFillAnswer",
                "number"
            )
        );
        if ($return) {
            return $result;
        }
        $this->displayJson(["value" => $result]);
    }

    /**
     * Delete data of this id
     * @param integer $qid
     * @param integer $srid
     * @param string $token
     */
    private function deleteData($qid, $srid, $token)
    {
        $ids = Yii::app()
            ->getRequest()
            ->getPost("ids");
        if (empty($ids)) {
            throw new CHttpException(400);
        }
        $dataHelper = $this->getDataHelper($qid, $srid, $token);
        $this->displayJson($dataHelper->deleteData($ids));
    }

    /**
     * Get the json source for autocomplete
     * @param integer $autocompleteqid
     * @param string $language
     * @param string $token
     */
    private function getAutocompleteSource($autocompleteqid, $language, $token)
    {
        /* @todo : control via survey id if allowed for this question */
        $aQuestionAttributes = QuestionAttribute::model()->getQuestionAttributes($autocompleteqid, $language);
        if (empty($aQuestionAttributes['relatedSpreadsheetResponseComplete'])) {
            throw new CHttpException(403, gT("Invalid question"));
        }
        $tokenUsage = $aQuestionAttributes['relatedSpreadsheetResponseCompleteTokenUsage'];
        if ($tokenUsage != 'no' && empty($token) && !$oSurvey->getIsAnonymized()) {
            throw new CHttpException(403, gT("Invalid without token"));
        }
        
        $sourceSurveysIds = explode(",", trim($aQuestionAttributes['relatedSpreadsheetResponseComplete']));
        $aSurveysCategories = self::getSurveysCategories($aQuestionAttributes['relatedSpreadsheetResponseCompleteCategories'], $language);
        $data = [];
        foreach ($sourceSurveysIds as $sourcesid) {
            $oSourceSurvey = Survey::model()->findByPk($sourcesid);
            if (empty($oSourceSurvey)) {
                continue;
            }
            if (!$oSourceSurvey->getHasResponsesTable()) {
                continue;
            }
            $titleQuestion = trim($aQuestionAttributes['relatedSpreadsheetResponseCompleteQuestion']);
            /* ID question */
            $criteriaTitleQuestion = new CDbCriteria();
            $criteriaTitleQuestion->select = ['sid','gid','qid','title'];
            $criteriaTitleQuestion->compare('sid', $sourcesid);
            $criteriaTitleQuestion->compare('title', $titleQuestion);
            $criteriaTitleQuestion->addInCondition('type', ["S","T","U","L","!","O","N","D","G","Y","*"]);
            $oTitleQuestion = Question::model()->find($criteriaTitleQuestion);
            if (!$oTitleQuestion) {
                continue;
            }
            /* @todo minimize the response request : but issue with source send with json */
            /* find the current id */
            $sTitle = $oTitleQuestion->sid . "X" . $oTitleQuestion->gid . "X" . $oTitleQuestion->qid;
            $oCriteria = new CDbCriteria();
            $oCriteria->select = ['id', App()->db->quoteColumnName($sTitle)];
            $oCriteria->condition = App()->db->quoteColumnName($sTitle) . "IS NOT NULL AND " .  App()->db->quoteColumnName($sTitle) . " <> ''";
            if ($tokenUsage != 'no' && !$oSourceSurvey->getIsAnonymized()) {
                if ($tokenUsage == 'group') {
                    if (version_compare(App()->getConfig('TokenUsersListAndManageAPI'), "0.14", ">=")) {
                        $tokenList = \TokenUsersListAndManagePlugin\Utilities::getTokensList($sourcesid, $token, false);
                    } elseif(class_exists('\responseListAndManage\Utilities')) {
                        $tokenList = \responseListAndManage\Utilities::getTokensList($sourcesid, $token, false);
                    }
                }
                if (empty($tokenList)) {
                    $oCriteria->compare("token", $token);
                } else {
                    $oCriteria->addInCondition("token", $tokenList);
                }
            }
            $oResponses = Response::model($sourcesid)->findAll($oCriteria);
            foreach ($oResponses as $oResponse) {
                if (count($sourceSurveysIds) > 1) {
                    $responseId = $sourcesid . '-' . $oResponse->id;
                } else {
                    $responseId = $oResponse->id;
                }
                $aCurrentReponse = array(
                    'id' => $responseId,
                    'name' => CHtml::encode($oResponse->getAttribute($sTitle)),
                );
                if (count($sourceSurveysIds) > 1) {
                    if (isset($aSurveysCategories[$sourcesid])) {
                        $aCurrentReponse['group'] = $aSurveysCategories[$sourcesid];
                    }
                }
                $data[] = $aCurrentReponse;
            }
        }
        $this->displayJson($data);
    }

    /**
     * Get a specific attribute by name
     * @param string $attribute
     * @param null|string $default
     * @return null|string
     */
    private function getQuestionAttribute($qid, $attribute, $default = null)
    {
        $ExtraSurveyAttribute = QuestionAttribute::model()->find(
            "qid = :qid AND attribute = :attribute",
            [":qid" => $qid, ":attribute" => $attribute]
        );
        if (empty($ExtraSurveyAttribute)) {
            return $default;
        }
        return $ExtraSurveyAttribute->value;
    }

    /**
     * get the data for this particular id
     * @param integer $surveyId
     * @param string $lang
     */
    private function updateData($qid, $srid, $token)
    {
        $answers = Yii::app()
            ->getRequest()
            ->getPost("answers");
        if (empty($answers)) {
            return;
        }
        $id = null;
        if (!empty($answers["id"])) {
            $id = $answers["id"];
            unset($answers["id"]);
        }
        unset($answers["submitdate"]);
        $dataHelper = $this->getDataHelper($qid, $srid, $token, true);
        $sForcedFillQuestion = trim(
            $this->getQuestionAttribute(
                $qid,
                "spreadsheetSurveyForcedFillQuestion"
            )
        );
        if (!empty($sForcedFillQuestion)) {
            $dataHelper->setColumnsAsReadonly($sForcedFillQuestion);
        }
        $returnData = $dataHelper->setData($id, $answers);
        /* Did we need to reset EM to current sid ?*/
        $this->displayJson($returnData);
    }

    /**
     * Validate if same token exist, create if not.
     * @param Survey $extraSurvey
     * @param Survey $thisSurvey
     * @return boolean (roken is valid)
     */
    private function validateToken($extraSurvey, $thisSurvey, $basetoken = null)
    {
        if (!$this->accessWithToken($extraSurvey)) {
            return false;
        }
        if (!$basetoken) {
            $basetoken = isset($_SESSION["survey_" . $thisSurvey->sid]["token"])
                ? $_SESSION["survey_" . $thisSurvey->sid]["token"]
                : null;
        }
        if (!$basetoken) {
            $this->log(
                sprintf("Unable to find token value for %s.", $thisSurvey->sid),
                "warning"
            );
            return false;
        }
        /* Find if token exist in new survey */
        $oToken = Token::model($extraSurvey->sid)->find("token = :token", [
            ":token" => $basetoken,
        ]);
        if (empty($oToken)) {
            $oBaseToken = Token::model($thisSurvey->sid)->find(
                "token = :token",
                [":token" => $basetoken]
            );
            if (empty($oBaseToken)) {
                $this->log(
                    sprintf(
                        "Unable to create token for %s, token for %s seems invalid.",
                        $extraSurvey->sid,
                        $thisSurvey->sid
                    ),
                    "error"
                );
                return false;
            }
            $oToken = Token::create($extraSurvey->sid);
            $disableAttribute = [
                "tid",
                "participant_id",
                "emailstatus",
                "blacklisted",
                "sent",
                "remindersent",
                "remindercount",
                "completed",
                "usesleft",
            ];
            $updatableAttribute = array_filter(
                $oBaseToken->getAttributes(),
                function ($key) use ($disableAttribute) {
                    return !in_array($key, $disableAttribute);
                },
                ARRAY_FILTER_USE_KEY
            );
            $oToken->setAttributes($updatableAttribute, false);
            $oToken->scenario =
                "allowinvalidemail"; /* Email get from another one */
            if ($oToken->save()) {
                $this->log(
                    sprintf(
                        "Auto create token %s for %s.",
                        $basetoken,
                        $extraSurvey->sid
                    ),
                    "info"
                );
                return true;
            } else {
                $this->log(
                    sprintf(
                        "Unable to create auto create token %s for %s.",
                        $basetoken,
                        $extraSurvey->sid
                    ),
                    "error"
                );
                $this->log(
                    CVarDumper::dumpAsString($oToken->getErrors()),
                    "info"
                );
                return false;
            }
        }
        return true;
    }

    /**
     * Return the data helper with restriction
     * This control security via qid (question id) and with session of survey
     * @param integer $qid
     * @return questionSpreadsheetSurveyData\helpers\dataHelper
     * @throw exception
     */
    private function getDataHelper($qid, $srid, $token, $debug = false)
    {
        $extraSurvey = $this->getSidFromQidValidity($qid);
        $oExtraSurvey = Survey::model()->findByPk($extraSurvey);
        // Check anonymous survey
        if ($oExtraSurvey->anonymized == "Y") {
            $token = null;
        }
        $this->subscribe("getPluginTwigPath", "addTwigPath");
        if (intval(App()->getConfig("versionnumber")) <= 3) {
            $dataHelper = new \questionSpreadsheetSurvey\helpers3LTS\dataHelper(
                $extraSurvey
            );
        } else {
            $dataHelper = new \questionSpreadsheetSurvey\helpers\dataHelper(
                $extraSurvey
            );
        }
        $sTokenUsage = trim(
            $this->getQuestionAttribute($qid, "spreadsheetSurveyTokenUsage")
        );
        if ($token) {
            if ($sTokenUsage != "no") {
                $dataHelper->setToken($token);
            }
            if ($sTokenUsage == "group") {
                $dataHelper->tokens = $this->getTokensList(
                    $extraSurvey,
                    $token
                );
            }
        }

        /** Restriction : Add extra filter params */
        /* current srid */
        $spreadsheetSurveyQuestionLink = trim(
            $this->getQuestionAttribute($qid, "spreadsheetSurveyQuestionLink")
        );
        if ($spreadsheetSurveyQuestionLink) {
            if (empty($srid)) {
                throw new CHttpException(400);
            }
            $dataHelper->setRestrictedTo([
                $spreadsheetSurveyQuestionLink => $srid,
            ]);
        }
        /* Manual other relation */
        $spreadsheetSurveyOtherField = $this->getQuestionAttribute(
            $qid,
            "spreadsheetSurveyOtherField"
        );
        if ($spreadsheetSurveyOtherField) {
            $spreadsheetSurveyOtherField = $this->fixOtherSpreadsheetField(
                $qid,
                $srid,
                $spreadsheetSurveyOtherField
            );
            $dataHelper->setRestrictedTo($spreadsheetSurveyOtherField);
        }
        /* Add forced column */
        $sForcedFillQuestion = trim(
            $this->getQuestionAttribute(
                $qid,
                "spreadsheetSurveyForcedFillQuestion"
            )
        );
        $sForcedFillValues = trim(
            $this->getQuestionAttribute(
                $qid,
                "spreadsheetSurveyForcedFillValues"
            )
        );
        if ($sForcedFillQuestion && $sForcedFillValues) {
            $aForcedFillValues = $this->fixValuePrefillSpreadsheetField(
                $qid,
                $srid,
                $sForcedFillValues
            );
            if (!empty($aForcedFillValues)) {
                $bDeleteNotForcedValues = boolval(
                    $this->getQuestionAttribute(
                        $qid,
                        "spreadsheetSurveyDeleteNotForcedValues",
                        true
                    )
                );
                $bSubmitted = boolval(
                    $this->getQuestionAttribute(
                        $qid,
                        "spreadsheetSurveyForcedSubmitted",
                        false
                    )
                );
                $dataHelper->setForcedDatas(
                    $sForcedFillQuestion,
                    $aForcedFillValues,
                    $bDeleteNotForcedValues,
                    $bSubmitted
                );
                $dataHelper->addEmptyLine = false;
            }
        }
        /* condition for readonly */
        $spreadsheetSurveyReadOnlyCondition = $this->getQuestionAttribute(
            $qid,
            "spreadsheetSurveyReadOnlyCondition"
        );
        if ($spreadsheetSurveyReadOnlyCondition) {
            $spreadsheetSurveyReadOnlyCondition = $this->fixReadOnlyCondition(
                $qid,
                $srid,
                $spreadsheetSurveyReadOnlyCondition
            );
            $dataHelper->setReadOnlyCondition($spreadsheetSurveyReadOnlyCondition);
        }
        /* condition for readonly on submitted */
        $spreadsheetSurveyReadOnlyConditionSubmitted = $this->getQuestionAttribute(
            $qid,
            "spreadsheetSurveyReadOnlyConditionSubmitted"
        );
        if ($spreadsheetSurveyReadOnlyConditionSubmitted) {
            $spreadsheetSurveyReadOnlyConditionSubmitted = $this->fixReadOnlyCondition(
                $qid,
                $srid,
                $spreadsheetSurveyReadOnlyConditionSubmitted
            );
            $dataHelper->setReadOnlyCondition($spreadsheetSurveyReadOnlyConditionSubmitted, true);
        }
        /* fixed line : no add line avaiable */
        $bFixedLines = $this->getQuestionAttribute(
            $qid,
            "spreadsheetSurveyFixedLines"
        );
        if ($bFixedLines) {
            $dataHelper->addEmptyLine = false;
        }
        /* The related data for autocomplete (and other system after) */
        $columns = $dataHelper->getColumns();
        foreach($columns as $column) {
            if (isset($column['qid'])) {
                $aQuestionAttributes = \QuestionAttribute::model()->getQuestionAttributes($column['qid']);
                if (!empty($this->getQuestionAttribute($column['qid'],'relatedSpreadsheetResponseComplete'))) {
                    $surveysId = explode(",", strval($this->getQuestionAttribute($column['qid'],'relatedSpreadsheetResponseComplete')));
                    if (count($surveysId) > 1) {
                        if ($relSID = trim($this->getQuestionAttribute($column['qid'],'relatedSpreadsheetResponseCompleteSID'))) {
                            $questionSidSGQ = $this->getColumnSgq($oExtraSurvey->sid, $relSID);
                            if ($questionSidSGQ) {
                                $dataHelper->autocompleteUpdates['sid'] = $questionSidSGQ;
                            }
                        }
                        if ($relSRID = trim($this->getQuestionAttribute($column['qid'],'relatedSpreadsheetResponseCompleteSRID'))) {
                            $questionSridSGQ = $this->getColumnSgq($oExtraSurvey->sid, $relSRID);
                            if ($questionSridSGQ) {
                                $dataHelper->autocompleteUpdates['srid'] = $questionSridSGQ;
                            }
                        }
                    }
                }
            }
        }
        return $dataHelper;
    }

    /**
     * Test is survey have token table
     * @param $iSurvey
     * @return boolean
     */
    private function hasToken($iSurvey)
    {
        if (
            version_compare(Yii::app()->getConfig("versionnumber"), "3", ">=")
        ) {
            return Survey::model()
                ->findByPk($iSurvey)
                ->getHasTokensTable();
        }
        return Survey::model()->hasTokens($iSurvey);
    }

    /**
     * Did this survey have token with reload available
     * @var \Survey
     * @return boolean
     */
    private function accessWithToken($oSurvey)
    {
        Yii::import("application.helpers.common_helper", true);
        return $oSurvey->anonymized != "Y" &&
            tableExists("{{tokens_" . $oSurvey->sid . "}}");
    }

    /**
     * Set the other field for current qid
     * @param integer $qid
     * @param integer $srid, current session saved id (used to keep id in session)
     * @param string $otherField to analyse
     * @param integer $surveyId in this survey
     * @return void
     */
    private function fixOtherSpreadsheetField($qid, $srid, $otherField)
    {
        $sessionValue = $this->getSessionValue($qid, $srid, "other");
        if (!is_null($sessionValue)) {
            return $sessionValue;
        }
        $aOtherFields = $this->settingsToColumnValue($otherField);
        $this->addSessionValue($qid, $srid, "other", $aOtherFields);
        return $aOtherFields;
    }

    /**
     * Set the readonly conitioon for current qid
     * @param integer $qid
     * @param integer $srid, current session saved id (used to keep id in session)
     * @param string  $readonlysetting to analyse
     * @param integer $surveyId in this survey
     * @return void
     */
    private function fixReadOnlyCondition($qid, $srid, $readonlysetting)
    {
        $sessionValue = $this->getSessionValue($qid, $srid, "readonlycondition");
        if (!is_null($sessionValue)) {
            return $sessionValue;
        }
        $aReadonlyFields = $this->settingsToColumnValue($readonlysetting);
        $this->addSessionValue($qid, $srid, "readonlycondition", $aReadonlyFields);
        return $aReadonlyFields;
    }

    /*
     * get column=>value to compare using string of setting
     * @param string
     * @return array()
     */
    private function settingsToColumnValue($setting)
    {
        $aFields = [];
        if (!empty($setting)) {
            $aFieldsLines = preg_split(
                '/\r\n|\r|\n/',
                $setting,
                -1,
                PREG_SPLIT_NO_EMPTY
            );
            foreach ($aFieldsLines as $fieldLine) {
                if (!strpos($fieldLine, ":")) {
                    continue; // Invalid line
                }
                $key = substr($fieldLine, 0, strpos($fieldLine, ":"));
                $value = substr(
                    $fieldLine,
                    strpos($fieldLine, ":") + 1
                );
                $value = self::ExpressionSpreadsheetProcessString($value);
                $aFields[$key] = $value;
            }
        }
        return $aFields;
    }

    /**
     * Set the other field for current qid
     * @param integer $qid
     * @param integer $srid, current session saved id (used to keep id in session)
     * @param string $otherField to analyse
     * @param integer $surveyId in this survey
     * @return void
     */
    private function fixValuePrefillSpreadsheetField(
        $qid,
        $srid,
        $forcedFillValues
    ) {
        $sessionValue = $this->getSessionValue($qid, $srid, "prefill");
        if (!is_null($sessionValue)) {
            return $sessionValue;
        }
        $aFillValues = [];
        if (!empty($forcedFillValues)) {
            $forcedFillValues = trim(
                self::ExpressionSpreadsheetProcessString($forcedFillValues)
            );
            $aForcedFillValues = preg_split(
                "/,|;|\|/",
                $forcedFillValues,
                -1,
                PREG_SPLIT_NO_EMPTY
            );
            foreach ($aForcedFillValues as $fillValue) {
                $fillValue = trim($fillValue);
                if ($fillValue !== "") {
                    $aFillValues[] = $fillValue;
                }
            }
        }
        $this->addSessionValue($qid, $srid, "prefill", $aFillValues);
        return $aFillValues;
    }

    /**
     * Get current survey id related after validation
     * @param integer $qid
     * @throw Exception
     * @return interger
     */
    private function getSidFromQidValidity($qid)
    {
        $oQuestion = Question::model()->find("qid = :qid", [":qid" => $qid]);
        if (empty($oQuestion)) {
            $this->throwAdaptedException(400, gT("Invalid question id"));
        }
        $extraSurvey = trim(
            $this->getQuestionAttribute($qid, "spreadsheetSurvey")
        );
        if (empty($extraSurvey)) {
            $this->throwAdaptedException(400, gT("Invalid question id"));
        }
        $oExtraSurvey = Survey::model()->findByPk($extraSurvey);
        if (empty($oExtraSurvey)) {
            $this->throwAdaptedException(400, gT("Invalid question id"));
        }
        return $extraSurvey;
    }

    /**
     * Return the list of token related by responseListAndManage
     * @todo : move this to a responseListAndManage helper
     * @param integer $surveyId
     * @param string $token
     * @return string[]|null
     */
    private function getTokensList($surveyId, $token)
    {
        $tokensList = [$token => $token];
        if (!$this->surveyHasToken($surveyId)) {
            return null;
        }
        if (
            version_compare(
                App()->getConfig("TokenUsersListAndManageAPI", 0),
                "0.14",
                ">="
            )
        ) {
            return \TokenUsersListAndManagePlugin\Utilities::getTokensList(
                $surveyId,
                $token,
                false
            );
        }
        if (!Yii::getPathOfAlias("responseListAndManage")) {
            return null;
        }
        if (!class_exists('\responseListAndManage\Utilities')) {
            return null;
        }
        return \responseListAndManage\Utilities::getTokensList(
            $surveyId,
            $token,
            false
        );
    }

    /**
     * Test is survey have token table
     * @param $iSurvey
     * @return boolean
     */
    private function surveyHasToken($iSurvey)
    {
        if (
            version_compare(Yii::app()->getConfig("versionnumber"), "3", ">=")
        ) {
            return Survey::model()
                ->findByPk($iSurvey)
                ->getHasTokensTable();
        }
        return Survey::model()->hasTokens($iSurvey);
    }

    /**
     * Unset all session for current qid and srid
     * @param integer $qid
     * @param integer $srid
     */
    private function unsetSession($qid, $srid)
    {
        $sessionQid = App()->session["questionSpreadSheet-$qid"];
        if (empty($sessionQid)) {
            $sessionQid = [];
        }
        unset($sessionQid[$srid]);
        $sessionQid[$srid] = [];
        App()->session["questionSpreadSheet-$qid"] = $sessionQid;
    }

    /**
     * Check if session is set for qid and srid
     * @param integer $qid
     * @param integer $srid
     * @return boolean
     */
    private function checkSession($qid, $srid)
    {
        $sessionQid = App()->session["questionSpreadSheet-$qid"];
        return isset($sessionQid[$srid]);
    }

    /**
     * Set a session value by type
     * @param integre $qid
     * @param integer $srid
     * @param string $type
     * @param mixed $value
     */
    private function addSessionValue($qid, $srid, $type, $value)
    {
        $sessionQid = App()->session["questionSpreadSheet-$qid"];
        if (empty($sessionQid[$srid])) {
            $sessionQid[$srid] = [];
        }
        $sessionQid[$srid][$type] = $value;
        App()->session["questionSpreadSheet-$qid"] = $sessionQid;
    }

    /**
     * Get a session value by type
     * @param integre $qid
     * @param integer $srid
     * @param string $type
     * @return mixed|null
     */
    private function getSessionValue($qid, $srid, $type)
    {
        $sessionQid = App()->session["questionSpreadSheet-$qid"];
        if (isset($sessionQid[$srid][$type])) {
            return $sessionQid[$srid][$type];
        }
        return null;
    }

    /*******************************************************
     * Common for a lot of plugin, helper for compatibility
     *******************************************************/

    /**
     * Process a string via expression manager (static way) anhd API independant
     * @param string $string
     * @param boolean $static
     * @return string
     */
    private static function ExpressionSpreadsheetProcessString(
        $string,
        $static = true
    ) {
        $replacementFields = [];
        if (intval(Yii::app()->getConfig("versionnumber")) < 3) {
            return \LimeExpressionManager::ProcessString(
                $string,
                null,
                $replacementFields,
                false,
                3,
                0,
                false,
                false,
                $static
            );
        }
        if (
            version_compare(
                Yii::app()->getConfig("versionnumber"),
                "3.6.2",
                "<"
            )
        ) {
            return \LimeExpressionManager::ProcessString(
                $string,
                null,
                $replacementFields,
                3,
                0,
                false,
                false,
                $static
            );
        }
        return \LimeExpressionManager::ProcessStepString(
            $string,
            $replacementFields,
            3,
            $static
        );
    }

    /**
     * get translation
     * @param string $string to translate
     * @param string escape mode
     * @param string language, current by default
     * @return string
     */
    private function translate(
        $string,
        $sEscapeMode = "unescaped",
        $sLanguage = null
    ) {
        if (is_callable([$this, "gT"])) {
            return $this->gT($string, $sEscapeMode, $sLanguage);
        }
        return $string;
    }

    /**
     * Throw a json data with header and without HTML log
     * @param mixed the data to thow
     * @return null
     */
    private function displayJson($data)
    {
        Yii::import("application.helpers.viewHelper"); // Allow log error of plugin on web
        viewHelper::disableHtmlLogging();
        header("Content-type: application/json; charset=utf-8");
        echo json_encode($data);
        Yii::app()->session["LEMsid"] = $this->currentSurveyId;
        Yii::app()->end();
    }

    /**
     * Throw Exception
     * Simple text if it's Ajax
     * @param integer $code
     * @param string $text
     * @throw Exception
     */
    private function throwAdaptedException($code, $message)
    {
        if (
            App()
                ->getRequest()
                ->getIsAjaxRequest()
        ) {
            /* see CErrorHandler::getHttpHeader */
            $textFromCode = [
                400 => "Bad Request",
                401 => "Unauthorized",
                402 => "Payment Required",
                403 => "Forbidden",
                404 => "Not Found",
                405 => "Method Not Allowed",
                406 => "Not Acceptable",
                407 => "Proxy Authentication Required",
                408 => "Request Timeout",
                409 => "Conflict",
                410 => "Gone",
                411 => "Length Required",
                412 => "Precondition Failed",
                413 => "Request Entity Too Large",
                414 => "Request-URI Too Long",
                415 => "Unsupported Media Type",
                416 => "Requested Range Not Satisfiable",
                417 => "Expectation Failed",
                418 => "I’m a teapot",
                422 => "Unprocessable entity",
                423 => "Locked",
                424 => "Method failure",
                425 => "Unordered Collection",
                426 => "Upgrade Required",
                428 => "Precondition Required",
                429 => "Too Many Requests",
                431 => "Request Header Fields Too Large",
                449 => "Retry With",
                450 => "Blocked by Windows Parental Controls",
                451 => "Unavailable For Legal Reasons",
                500 => "Internal Server Error",
                501 => "Not Implemented",
                502 => "Bad Gateway",
                503 => "Service Unavailable",
                504 => "Gateway Timeout",
                505 => "HTTP Version Not Supported",
                507 => "Insufficient Storage",
                509 => "Bandwidth Limit Exceeded",
                510 => "Not Extended",
                511 => "Network Authentication Required",
            ];
            if (isset($textFromCode[$code])) {
                $httpVersion = Yii::app()->request->getHttpVersion();
                header(
                    "HTTP/$httpVersion {$code} {$textFromCode[$code]}",
                    true,
                    $code
                );
                echo $message;
                App()->end();
            }
        }
        throw new CHttpException($code, $message);
    }

    /**
     * Return the question column accoring to sid and title
     * @param integer $sid
     * @param string title
     * @return string|false
     */
    private static function getColumnByTitle($sid, $title)
    {
        $oQuestion = Question::model()->find(
            "sid=:sid and title=:title and parent_qid=0",
            array(
                ":sid" => $sid,
                ":title" => $title
            )
        );
        if (empty($oQuestion)) {
            return false;
        }
        if (!in_array($oQuestion->type, array("5","D","G","I","L","N","O","S","T","U","X","Y","!","*"))) {
            return false;
        }
        return "{$oQuestion->sid}X{$oQuestion->gid}X{$oQuestion->qid}";
    }

    /**
     * Transform selectQuestionByResponseCategoriesForSurveys attribute to array in language
     * Line by line, separated by a :
     * @param string[] The attribute
     * @param string language to get
     * @return array
     */
    private static function getSurveysCategories($attribute, $language)
    {
        if (empty($attribute[$language])) {
            return array();
        }
        $attribute = trim($attribute[$language]);
        $aCategoriesLines = preg_split('/\r\n|\r|\n/', $attribute, -1, PREG_SPLIT_NO_EMPTY);
        $aSurveysCategories = array();
        foreach ($aCategoriesLines as $aCategoriesLine) {
            if (!strpos($aCategoriesLine, ":")) {
                continue; // Invalid line
            }
            $survey = substr($aCategoriesLine, 0, strpos($aCategoriesLine, ":"));
            $text = substr($aCategoriesLine, strpos($aCategoriesLine, ":") + 1);
            $aSurveysCategories[$survey] = $text;
        }
        return $aSurveysCategories;
    }

    /**
     * get the column sqg
     * @param string
     * @param $title string
     * @return null|string
     */
    private function getColumnSgq($sid, $title)
    {
        /* restrict to single element */
        $oQuestion = Question::model()->find([
            'select' => ['sid', 'gid', 'qid'],
            'condition' => "sid = :sid and title = :title and parent_qid = 0",
            'params' => [
                ":sid" => $sid,
                ":title" => $title,
            ]
        ]);
        if (!$oQuestion) {
            return null;
        }
        return $oQuestion->sid . "X" . $oQuestion->gid . "X" . $oQuestion->qid;
    }

    /**
     * @inheritdoc adding string, by default current event
     * @param string $message
     * @param string $level From CLogger, defaults to CLogger::LEVEL_TRACE
     * @param string $logDetail
     */
    public function log(
        $message,
        $level = \CLogger::LEVEL_TRACE,
        $logDetail = null
    ) {
        if (!$logDetail && $this->getEvent()) {
            $logDetail = $this->getEvent()->getEventName();
        } // What to put if no event ?
        if ($logDetail) {
            $logDetail = "." . $logDetail;
        }
        $category = get_class($this);
        \Yii::log($message, $level, "plugin." . $category . $logDetail);
    }
}
