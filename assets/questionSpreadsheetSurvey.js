/**
 * Action for questionSpreadsheetSurvey
 * @author Denis Chenu
 * @licence MIT
 * @version 1.5.3
 */
var questionSpreadsheetSurvey = {
	GlobalOptions: {},
	options: {},
	language: {},
	/**
	 * set spreadsheet to needed element
	 * @var string : element id
	 * @var Ovject : options
	 */
	setSpreadsheet: function(elementId, options) {
		this.options = options;
		var jexcelOpts = $.extend({},
			options.jexcel, {
				allowInsertColumn: false,
				allowDeleteColumn: false,
				allowRenameColumn: false,
				about: "jExcel CE Spreadsheet\nVersion 3.6.1\nAuthor: Paul Hodel \nLimeSurvey integration\nVersion 1.2.1\nAuthor: Denis Chenu",
				onload: questionSpreadsheetSurvey.jexcelloaded,
				defaultColWidth: "*",
				defaultColAlign: "none",
				oneditionend: questionSpreadsheetSurvey.editionEnd,
				onchange: questionSpreadsheetSurvey.changeData,
				onpaste: questionSpreadsheetSurvey.pasteDatas,
				onafterchanges: questionSpreadsheetSurvey.updateAllDatas,
				onbeforeinsertcolumn: function(instance) {
					return false;
				},
				onbeforedeletecolumn: function(instance) {
					return false;
				},
				//insertedRow : questionSpreadsheetSurvey.createData,
				onbeforedeleterow: questionSpreadsheetSurvey.deleteData,
				onbeforeinsertrow: function(instance) {
					return true;
				}
			}
		);
		var table = $("#" + elementId).jexcel(jexcelOpts);
	},
	jexcelloaded: function(element, instance) {
		$(element).find("table").addClass("table table-condensed table-bordered");
		if ($(".navbar-fixed-top").length) {
			var fixedHeaderHeight = $(".navbar-fixed-top").height();
			$(element).find("thead > tr > td").each(function() {
				$(this).css("top", fixedHeaderHeight + "px");
				$(this).css("z-index", 1040);
			});
			// window resize ?
		}
		/* this was current jexcel */
		var columnsDef = this.columns;
		jQuery.each(columnsDef, function(index, column) {
			x = index + 2; // 1st column + start by 0
			if (column.className) {
				$(element).find("tr td:nth-child(" + x + ")").addClass(column.className);
			}
			if (column.titleattr) {
				$(element).find("thead tr td:nth-child(" + x + ")").attr('title', column.titleattr);
			} else {
				var sanitized = $(element).find("thead tr td:nth-child(" + x + ")").text();
				$(element).find("thead tr td:nth-child(" + x + ")").attr('title', sanitized);
			}
		});
		$(element).find("table tbody").find("tr").each(function(index) {
			var srid = $(this).find('[data-x="0"]').text();
			var state = $(this).find('[data-x="1"]').text();
			var completed = $(this).find('[data-x="2"]').text();
			if (srid && !completed) {
				$(this).addClass("warning");
			} else {
				$(this).removeClass("warning");
			}
			if (srid && state == 'readonly') {
				$(this).find("[data-x]").addClass("readonly");
			}
		});
		$(element).find("table tbody").find("td.dropdown").prepend("<i class='dropdown-open' aria-hidden='true'></i>");
		$(element).on('click', 'td.dropdown .dropdown-open', function() {
			var columnId = $(this).parent(".dropdown").data("x");
			var rowId = $(this).parent(".dropdown").data("y");
			instance.openEditor(instance.records[rowId][columnId], true);
		});
		questionSpreadsheetSurvey.updateInput(element);
		$(element).trigger('jexcelloaded');
	},
	updateAllDatas: function(instance, records) {
		if (!$(instance).hasClass("speadsheet-working")) {
			$(instance).addClass("speadsheet-working");
		}
		var lines = [];
		var lastIndex = $(instance).find("tbody tr").length - 1;
		$(instance).find("tbody tr").each(function(index) {
			if (!$(this).data('setValue') && $(this).data('valuesUpdated')) {
				lines.push($(this).data('y'));
				$(this).addClass("line-working");
				$(this).data('setValue', true);
			} else {}
			if (index == lastIndex && lines.length) {
				questionSpreadsheetSurvey.setLoading();
				questionSpreadsheetSurvey.loopSetData(instance, lines);
			}
		});
	},
	changeData: function(instance, cell, x, y, value) {
		if ($(instance).find("tbody tr").eq(y).data('valuesUpdated')) {
			return;
		}
		if ($(instance).find("tbody tr").eq(y).data('setValue')) {
			return;
		}
		var jexcelLastRow = (instance.jexcel.options.data.length - 1);
		$(instance).find("tbody tr").eq(y).data('valuesUpdated', true);
		if (y == jexcelLastRow) {
			instance.jexcel.insertRow();
			questionSpreadsheetSurvey.addDropdownOpen(instance);
		}
		//questionSpreadsheetSurvey.updateInput(instance);
	},
	editionEnd: function(instance, cell, x, y, value) {
		var jexcelLastRow = (instance.jexcel.options.data.length - 1);
		if (y == jexcelLastRow) {
			instance.jexcel.insertRow();
			questionSpreadsheetSurvey.addDropdownOpen(instance);
		}
	},
	pasteDatas: function(instance, records) {
		var jexcelLastRow = (instance.jexcel.options.data.length - 1);
		var updatedLastRow = 0;
		$.each(records, function(index, record) {
			if (typeof record !== 'undefined') {
				updatedLastRow = record.row;
			}
		});
		if (updatedLastRow == jexcelLastRow) {
			instance.jexcel.insertRow();
			questionSpreadsheetSurvey.addDropdownOpen(instance);
		}
	},
	/* The really setData function : set whole line */
	loopSetData: function(instance, lines) {
		if (!lines.length) {
			if (!$(instance).find("tr.line-working").length) {
				$(instance).removeClass("speadsheet-working");
				if (!$(".speadsheet-working").length) {
					questionSpreadsheetSurvey.unsetLoading();
				}
				questionSpreadsheetSurvey.updateInput(instance);
			}
			return;
		}
		var y = lines[0];
		lines.shift();
		var rowData = instance.jexcel.getRowData(y);
		var colData = $(instance).data('columns');
		var answersData = {};
		$.each(colData, function(index, value) {
			answersData[value.id] = rowData[index];
		});
		var postData = $.extend({}, questionSpreadsheetSurvey.GlobalOptions.urlPostBase, {
			answers: answersData
		});
		var tableLine = $(instance).find("tbody tr").eq(y);
		$.ajax({
				async: true,
				url: $(instance).data('updateurl'),
				type: 'POST',
				data: postData,
				dataType: 'json',
			})
			.done(function(jsonData, textStatus, jqXHR) {
				// Update data 
				var updatesValues = [];
				$.each(colData, function(index, value) {
					updatesValues.push(jsonData.response[value.id]);
				});
				for (var i = 0; i < instance.jexcel.headers.length; i++) {
					// Update cell
					var columnName = jexcel.getColumnNameFromId([i, y]);
					// Set value
					if (updatesValues[i] != null) {
						instance.jexcel.setValue(columnName, updatesValues[i], true);
					} else {
						instance.jexcel.setValue(columnName, "", true);
					}
				}
				lineStatus = jsonData.status;
				if (lineStatus.mandViolation || !lineStatus.valid) {
					$(tableLine).addClass("warning");
					$(tableLine).removeClass("danger");
				} else {
					$(tableLine).removeClass("warning");
					$(tableLine).removeClass("danger");
				}
				$(tableLine).find("td.dropdown").each(function() {
					if (!$(this).find(".dropdown-open").length) {
						$(this).prepend("<i class='dropdown-open' aria-hidden='true'></i>");
					}
				});
				$(tableLine).data('valuesUpdated', false);
				$(tableLine).removeClass("line-working");
				questionSpreadsheetSurvey.loopSetData(instance, lines);
			})
			.fail(function(jqXHR, textStatus, errorThrown) {
				$(instance).find("tbody tr").eq(y).removeClass("warning");
				$(instance).find("tbody tr").eq(y).addClass("danger");
				questionSpreadsheetSurvey.showAjaxError(jqXHR, textStatus, errorThrown);
				questionSpreadsheetSurvey.unsetLoading();
				$(this).data('setValue', true);
			})
			.always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {
				$(tableLine).removeClass("line-working");
				$(tableLine).data('setValue', false);
				//questionSpreadsheetSurvey.updateInput(instance);
			});
	},
	deleteData: function(instance, rowNumber, numOfRows) {
		var currentRow = rowNumber;
		var idList = [];
		while (currentRow < numOfRows + rowNumber) {
			var rowData = instance.jexcel.getRowData(currentRow);
			if (rowData && rowData[0]) {
				idList.push(rowData[0]);
			}
			currentRow++;
		}
		if (!idList.length) {
			return;
		}
		var postData = $.extend({}, questionSpreadsheetSurvey.GlobalOptions.urlPostBase, {
			ids: idList
		});
		result = true;
		$.ajax({
			async: true,
			url: $(instance).data('deleteurl'),
			type: 'POST',
			data: postData,
			dataType: 'json',
			success: function(jsonData, status) {
				// Nothing to do : excepot update the input
				questionSpreadsheetSurvey.updateInput(instance);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				questionSpreadsheetSurvey.showAjaxError(jqXHR, textStatus, errorThrown);
				result = false;
			}
		});
		return result;
	},
	updateInput: function(element) {
		var resultUrl = $(element).data('resulturl');
		if (!resultUrl) {
			return;
		}
		var inputid = $(element).data('inputid');
		var postData = $.extend({}, questionSpreadsheetSurvey.GlobalOptions.urlPostBase);
		$.ajax({
			async: true,
			url: resultUrl,
			type: 'GET',
			data: postData,
			dataType: 'json',
			success: function(jsonData, status) {
				$("#" + inputid).val(jsonData.value).trigger("change");
				questionSpreadsheetSurvey.addDropdownOpen(element);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				questionSpreadsheetSurvey.showAjaxError(jqXHR, textStatus, errorThrown);
			}
		});
	},
	addDropdownOpen: function(element) {
		$(element).find("table tbody").find("td.dropdown").each(function() {
			if (!$(this).find(".dropdown-open").length) {
				$(this).prepend("<i class='dropdown-open' aria-hidden='true'></i>");
			}
		});
	},
	setLoading: function() {
		if (!$("#speadsheet-ajax-loading").length) {
			$("<div id='speadsheet-ajax-loading'></div>").appendTo("body");
		}
	},
	unsetLoading: function() {
		$("#speadsheet-ajax-loading").remove();
	},
	showAjaxError: function(jqXHR, textStatus, errorThrown) {
		if (typeof questionSpreadsheetSurvey.alertShown !== 'undefined') {
			return;
		}
		questionSpreadsheetSurvey.alertShown = true;
		var errorMessage = jqXHR.responseText;
		if (errorMessage.indexOf("DOCTYPE html") < 0 && errorMessage.indexOf("<h1>") < 0) {
			alert(errorMessage);
		} else {
			/* CSRF issue : can send a session issue */
			if (errorMessage.indexOf("CSRF") < 0) {
				alert(questionSpreadsheetSurvey.language["An unknow error happen, please reload."]);
			} else {
				alert(questionSpreadsheetSurvey.language["The CSRF token could not be verified."]);
			}
		}
	}
};
